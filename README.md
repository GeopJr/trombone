<div align="center">
  <img alt="" width="160" src="./data/icons/color.svg">
</div>
<div align="center"><h1>Trombone</h1></div>
<div align="center"><h3>Join the Discussion</h3></div>
<div align="center">
  <br />
    <a href="./CODE_OF_CONDUCT.md"><img src="https://img.shields.io/badge/Code%20of%20Conduct-GNOME-f5c211.svg?style=for-the-badge&labelColor=f9f06b" alt="Contributor Covenant v2.1" /></a>
    <a href="./LICENSE"><img src="https://img.shields.io/badge/LICENSE-GPL--3.0-f5c211.svg?style=for-the-badge&labelColor=f9f06b" alt="License GPL-3.0" /></a>
    <a href='https://stopthemingmy.app'><img width='193.455' alt='Please do not theme this app' src='https://stopthemingmy.app/badge.svg'/></a>
</div>

<div align="center">
    <img alt="" src="https://i.imgur.com/ggplEXU.png">
</div>

# Install

### Makefile

```
$ make
$ make install
```

### GNOME Builder

- Clone
- Open in GNOME Builder

# Acknowledgements

- Trombone is heavily based on Tuba's codebase

# Contributing

1. Read the [Code of Conduct](./CODE_OF_CONDUCT.md)
1. Fork it ( https://gitlab.gnome.org/GeopJr/trombone/-/forks/new )
1. Create your feature branch (git checkout -b my-new-feature)
1. Commit your changes (git commit -am 'Add some feature')
1. Push to the branch (git push origin my-new-feature)
1. Create a new Pull Request
