public class Trombone.Widgets.Avatar : Gtk.Button {
	public API.InstanceAccount? account {
		set {
			on_invalidated (value);
		}
	}

	public int size {
		get { return avatar.size; }
		set { avatar.size = value; }
	}

	public Gdk.Paintable? custom_image {
		get { return avatar.custom_image; }
	}

	protected Adw.Avatar? avatar {
		get { return child as Adw.Avatar; }
	}

	string? _avatar_url = null;
	public string? avatar_url {
		get {
			return _avatar_url;
		}

		set {
			_avatar_url = value;

			if (value != null) {
				Trombone.Helper.Image.request_paintable (value, null, null, on_cache_response);
			} else {
				avatar.custom_image = null;
			}
		}
	}

	string _fallback_text = "d";
	public string fallback_text {
		get {
			return _fallback_text;
		}

		set {
			_fallback_text = value;
			avatar.text = value;
			avatar.show_initials = true;
		}
	}

	construct {
		child = new Adw.Avatar (48, null, true);
		halign = valign = Gtk.Align.CENTER;
		css_classes = { "flat", "circular", "image-button", "ttl-flat-button" };

		on_invalidated ();
	}

	void on_invalidated (API.InstanceAccount? account = null) {
		if (account == null) {
			avatar.text = fallback_text;
			avatar.show_initials = false;
		} else {
			avatar.text = account.name;
			avatar.show_initials = true;
            if (account.avatar != null)
			    Trombone.Helper.Image.request_paintable (account.avatar, null, null, on_cache_response);
		}
	}

	void on_cache_response (Gdk.Paintable? data) {
		avatar.custom_image = data;
	}
}
