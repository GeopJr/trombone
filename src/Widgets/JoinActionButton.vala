public class Trombone.Widgets.JoinActionButton : Widgets.PostActionButton {
    public enum SUBSCRIPTION {
        SUBSCRIBED,
        NOT_SUBSCRIBED,
        PENDING;

        public static SUBSCRIPTION from_string (string status) {
            switch (status.up ()) {
                case "SUBSCRIBED":
                    return SUBSCRIBED;
                case "PENDING":
                    return PENDING;
                default:
                    return NOT_SUBSCRIBED;
            }
        }

        public string to_string () {
            switch (this) {
                case SUBSCRIBED:
                    return _("Joined");
                case PENDING:
                    return _("Pending");
                default:
                    return _("Join");
            }
        }

        public bool can_subscribe () {
            switch (this) {
                case SUBSCRIBED:
                case PENDING:
                    return false;
                default:
                    return true;
            }
        }
    }

    public int64 community_id { get; set; default = -1; }

    private SUBSCRIPTION _subscription = SUBSCRIPTION.NOT_SUBSCRIBED;
    public SUBSCRIPTION subscription {
        get {
            return _subscription;
        }

        set {
            _subscription = value;
            this.label = value.to_string ();
            active = !subscription.can_subscribe ();
        }
    }

	protected override void update_button_style (bool value = active) {
		if (value) {
			remove_css_class ("suggested-action");
			add_css_class ("enabled");
		} else {
			add_css_class ("suggested-action");
			remove_css_class ("enabled");
		}
	}

    construct {
        this.clicked.connect (process);
    }

    public void process () {
        if (community_id == -1) return;
        block_clicked ();

        var builder = new Json.Builder ();
		builder.begin_object ();

		if (accounts.active.api_v18) {
            builder.set_member_name ("auth");
		    builder.add_string_value (accounts.active.secret);
        }

		builder.set_member_name ("community_id");
		builder.add_int_value (community_id);

        builder.set_member_name ("follow");
		builder.add_boolean_value (subscription.can_subscribe ());

		builder.end_object ();

        var msg = new Request.POST (@"https://$(accounts.active.instance)/api/v3/community/follow").body_json (builder);
        if (!accounts.active.api_v18) {
			msg.with_account (accounts.active);
		}
		msg.await.begin ((o, res) => {
			try {
				msg.await.end (res);

				var parser = Network.get_parser_from_inputstream (msg.response_body);
        		var subscribed_obj = network.parse (parser).get_object_member ("community_view").get_string_member ("subscribed");
                warning (subscribed_obj);
				subscription = SUBSCRIPTION.from_string (subscribed_obj);

				debug ("Follow complete");
			} catch (Error e) {
				warning (@"Couldn't perform follow on a Community: $(e.message)");

				var dlg = app.inform (_("Network Error"), e.message);
				dlg.present ();
			}

			unblock_clicked ();
		});
    }
}
