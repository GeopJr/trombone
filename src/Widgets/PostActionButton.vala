public class Trombone.Widgets.PostActionButton : Gtk.Button {
	private string _default_icon_name = "";
	public string default_icon_name {
		get {
			return _default_icon_name;
		}

		set {
			_default_icon_name = value;
			update_button_style ();
		}
	}

	public string? active_icon_name { get; construct set; default = null; }
	public bool working { get; private set; default = false; }

	private bool _active = false;
	public bool active {
		get {
			return _active;
		}

		set {
			_active = value;
			update_button_style (value);
		}
	}

	protected virtual void update_button_style (bool value = active) {
		if (value) {
			remove_css_class ("flat");
			add_css_class ("enabled");
			this.icon_name = active_icon_name ?? default_icon_name;
		} else {
			add_css_class ("flat");
			remove_css_class ("enabled");
			this.icon_name = default_icon_name;
		}
	}

	public void block_clicked () {
		working = true;
	}

	public void unblock_clicked () {
		working = false;
	}

	public PostActionButton.with_icon_name (string icon_name) {
		Object (
			default_icon_name: icon_name
		);
	}
}
