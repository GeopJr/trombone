public class Trombone.Widgets.CommunityRow : Adw.ActionRow {
    Widgets.Avatar avi;
    Widgets.JoinActionButton joinbtn;

    construct {
        avi = new Widgets.Avatar () {
            size = 32,
            can_focus = false,
            can_target = false
        };
        this.add_prefix (avi);

        joinbtn = new Widgets.JoinActionButton () {
            visible = false,
            valign = Gtk.Align.CENTER
        };
        this.add_suffix (joinbtn);
    }

    private API.Objects.Lemmy.Community? community = null;
    public void from_explore (API.Objects.Lemmy.CommunityExploreWrapper.CommunityExplore explore) {
        joinbtn.community_id = explore.community.id;
        joinbtn.subscription = Widgets.JoinActionButton.SUBSCRIPTION.from_string (explore.subscribed);
        joinbtn.visible = true;

        if (explore.community.icon != null && explore.community.icon != "") {
            avi.avatar_url = explore.community.icon;
        } else {
            avi.fallback_text = explore.community.name;
        }

        this.title = explore.community.handle;
        this.subtitle = @"<span segment=\"word\"><b>$(Trombone.Units.shorten (explore.counts.subscribers))</b> Subscribers</span>";
        community = explore.community;
    }
}
