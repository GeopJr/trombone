[GtkTemplate (ui = "/dev/geopjr/Trombone/widgets/post.ui")]
public class Trombone.Widgets.Post : Gtk.Box {
	[GtkChild] unowned Widgets.Avatar avatar;
	[GtkChild] unowned Gtk.Label header_name;
	//  [GtkChild] unowned Widgets.JoinActionButton joinbtn;

	[GtkChild] unowned Gtk.Label title;
	[GtkChild] unowned Gtk.Label body;
	[GtkChild] unowned Gtk.Button attachment;
	[GtkChild] unowned Gtk.Image thumbnail;

	[GtkChild] unowned Widgets.PostActionButton upvote;
	[GtkChild] unowned Gtk.Label score;
	[GtkChild] unowned Widgets.PostActionButton downvote;
	[GtkChild] unowned Gtk.Box comments_box;
	[GtkChild] unowned Gtk.Image comments_icon;
	[GtkChild] unowned Gtk.Label comments;
	[GtkChild] unowned Gtk.Label date;

	protected SimpleAction save_action;
	protected SimpleActionGroup actions { get; set; default = new SimpleActionGroup (); }
	private string action_url { get; set; }
	private const GLib.ActionEntry[] ACTION_ENTRIES = {
		{"copy-url", copy_url},
		{"open-in-browser", open_in_browser},
		{"save", save},
	};

	construct {
		actions.add_action_entries (ACTION_ENTRIES, this);

		save_action = new SimpleAction.stateful ("save", null, false);
		actions.add_action (save_action);

		this.insert_action_group ("post", actions);
	}

	private int64 _total_score = 0;
	private int64 total_score {
		get {
			return _total_score;
		}

		set {
			_total_score = value;
			score.label = Trombone.Units.shorten (value);
			score.tooltip_text = _("%lld Score").printf (value);
		}
	}

	private int64 _total_comments = 0;
	private int64 total_comments {
		get {
			return _total_comments;
		}

		set {
			_total_comments = value;
			comments.label = Trombone.Units.shorten (value);
			comments_box.tooltip_text = _("%lld Comments").printf (value);
			comments_icon.icon_name = value > 1 ? "chat-bubbles-empty-inverted-symbolic" : "chat-symbolic";
		}
	}

	private bool _is_saved = false;
	private bool is_saved {
		get { return _is_saved; }
		set {
			_is_saved = value;
			save_action.set_state (_is_saved);
		}
	}

	private int64 post_id { get; private set; }
	private string post_attachment_url { get; private set; }
	public void from_post_wrapper (API.Objects.Lemmy.PostWrapper t_post_wrapper, bool is_community = false) {
		action_url = t_post_wrapper.post.ap_id;
		is_saved = t_post_wrapper.saved;

		upvote.clicked.connect (on_upvote_btn_clicked);
		downvote.clicked.connect (on_downvote_btn_clicked);
		upvote.notify["active"].connect (update_vote_buttons_states);
		downvote.notify["active"].connect (update_vote_buttons_states);

		if (
			accounts.active.instance_info != null
			&& accounts.active.instance_info.site_view != null
			&& accounts.active.instance_info.site_view.local_site != null
			&& accounts.active.instance_info.site_view.local_site.enable_downvotes == false
		) {
			downvote.visible = false;
			comments_box.margin_start = 12;
		}

		switch (t_post_wrapper.voted) {
			case API.Objects.Lemmy.PostWrapper.VOTE.UP:
				upvote.active = true;
				downvote.active = false;
				break;
			case API.Objects.Lemmy.PostWrapper.VOTE.DOWN:
				downvote.active = true;
				upvote.active = false;
				break;
			default:
				downvote.active = false;
				upvote.active = false;
				break;
		}

		post_id = t_post_wrapper.post.id;
		if (is_community) {
			header_name.label = @"<b><span segment=\"word\">u/$(t_post_wrapper.creator.name)</span></b>";
			if (t_post_wrapper.creator.avatar != null && t_post_wrapper.creator.avatar != "") {
				avatar.avatar_url = t_post_wrapper.creator.avatar;
			} else {
				avatar.fallback_text = t_post_wrapper.creator.name;
			}

			avatar.clicked.connect (() => {
				app.main_window.open_view (new Views.Profile (t_post_wrapper.creator));
			});
		} else {
			header_name.label = @"<b><span segment=\"word\">$(t_post_wrapper.community.title)</span></b> · <span segment=\"word\">$(t_post_wrapper.community.handle)</span>";
			if (t_post_wrapper.community.icon != null && t_post_wrapper.community.icon != "") {
				avatar.avatar_url = t_post_wrapper.community.icon;
			} else {
				avatar.fallback_text = t_post_wrapper.community.title;
			}

			avatar.clicked.connect (() => {
				app.main_window.open_view (new Views.Community (t_post_wrapper.community));
			});
		}

		date.label = DateTime.humanize (@"$(t_post_wrapper.post.published)Z");
		date.tooltip_text = t_post_wrapper.post.published;

		update_title (t_post_wrapper.post.name, t_post_wrapper.post.url);
		if (t_post_wrapper.post.body != null && t_post_wrapper.post.body != "") {

			var clean_body = GLib.Markup.escape_text (t_post_wrapper.post.body.replace ("\n", " "));

			body.label = MarkdownUtils.pango_from_string (clean_body);
			body.visible = true;
		}

		if (t_post_wrapper.post.thumbnail_url != null && t_post_wrapper.post.thumbnail_url != "") {
			attachment.visible = true;
			post_attachment_url = t_post_wrapper.post.thumbnail_url;
			Trombone.Helper.Image.request_paintable (t_post_wrapper.post.thumbnail_url, null, null, (data) => {
				thumbnail.paintable = data;
			});
		}

		total_score = t_post_wrapper.counts.score;
		total_comments = t_post_wrapper.counts.comments;
		attachment.clicked.connect (open_attachment);
	}

	private int64 comment_id { get; private set; default=-1; }
	public void from_comment_wrapper (API.Objects.Lemmy.CommentWrapper t_comment_wrapper) {
		action_url = t_comment_wrapper.comment.ap_id;
		is_saved = t_comment_wrapper.saved;

		upvote.clicked.connect (on_upvote_btn_clicked);
		downvote.clicked.connect (on_downvote_btn_clicked);
		upvote.notify["active"].connect (update_vote_buttons_states);
		downvote.notify["active"].connect (update_vote_buttons_states);

		if (
			accounts.active.instance_info != null
			&& accounts.active.instance_info.site_view != null
			&& accounts.active.instance_info.site_view.local_site != null
			&& accounts.active.instance_info.site_view.local_site.enable_downvotes == false
		) {
			downvote.visible = false;
			comments_box.margin_start = 12;
		}

		switch (t_comment_wrapper.voted) {
			case API.Objects.Lemmy.PostWrapper.VOTE.UP:
				upvote.active = true;
				downvote.active = false;
				break;
			case API.Objects.Lemmy.PostWrapper.VOTE.DOWN:
				downvote.active = true;
				upvote.active = false;
				break;
			default:
				downvote.active = false;
				upvote.active = false;
				break;
		}

		comment_id = t_comment_wrapper.comment.id;
		header_name.label = @"<b><span segment=\"word\">u/$(t_comment_wrapper.creator.name)</span></b>";
		if (t_comment_wrapper.creator.avatar != null && t_comment_wrapper.creator.avatar != "") {
			avatar.avatar_url = t_comment_wrapper.creator.avatar;
		} else {
			avatar.fallback_text = t_comment_wrapper.creator.name;
		}

		avatar.clicked.connect (() => {
			t_comment_wrapper.creator.open ();
		});

		date.label = DateTime.humanize (@"$(t_comment_wrapper.comment.published)Z");
		date.tooltip_text = t_comment_wrapper.comment.published;

		if (t_comment_wrapper.comment.content != null && t_comment_wrapper.comment.content != "") {
			var clean_body = GLib.Markup.escape_text (t_comment_wrapper.comment.content);

			body.label = MarkdownUtils.pango_from_string (clean_body);
			body.visible = true;
			body.lines = -1;
		}

		total_score = t_comment_wrapper.counts.score;
		comments_box.visible = false;
		title.visible = false;
	}

	private void update_title (string text, string? url = null) {
		if (url == null || Attachment.MediaType.from_url (url) != Attachment.MediaType.UNKNOWN) {
			title.label = text;
			return;
		}

		string host_string = "";
		try {
			host_string = @"[$(Uri.parse (url, UriFlags.NONE).get_host ())]";
		} catch {
		}

		title.use_markup = true;
		title.label = @"<a href=\"$(GLib.Markup.escape_text (url))\">$text</a> <sub>$host_string</sub>";
	}

	private void open_attachment () {
		app.main_window.show_media_viewer (
			post_attachment_url,
			Trombone.Attachment.MediaType.from_url (post_attachment_url),
			thumbnail.paintable,
			null,
			attachment,
			true // i guess???
		);
	}

	private void update_vote_buttons_states () {
		score.remove_css_class ("upvote");
		score.remove_css_class ("downvote");

		if (upvote.active && downvote.active) {
			downvote.sensitive = true;
			upvote.sensitive = true;
			return;
		}

		downvote.sensitive = !upvote.active;
		upvote.sensitive = !downvote.active;

		if (upvote.active) score.add_css_class ("upvote");
		if (downvote.active) score.add_css_class ("downvote");
	}

	private void on_downvote_btn_clicked (Gtk.Button btn) {
		var vote_btn = on_vote_btn_clicked (btn);
		if (vote_btn == null) return;
		int64 score_diff = vote_btn.active ? -1 : 0;
		vote (vote_btn, score_diff);
	}

	private void on_upvote_btn_clicked (Gtk.Button btn) {
		var vote_btn = on_vote_btn_clicked (btn);
		if (vote_btn == null) return;
		int64 score_diff = vote_btn.active ? 1 : 0;
		vote (vote_btn, score_diff);
	}

	private Widgets.PostActionButton? on_vote_btn_clicked (Gtk.Button btn) {
		var vote_btn = btn as Widgets.PostActionButton;
		if (vote_btn.working) return null;
		vote_btn.block_clicked ();
		vote_btn.active = !vote_btn.active;

		return vote_btn;
	}

	private void vote (Widgets.PostActionButton vote_btn, int64 score) {
		bool is_comment = comment_id != -1;

		var builder = new Json.Builder ();
		builder.begin_object ();

		if (accounts.active.api_v18) {
        	builder.set_member_name ("auth");
			builder.add_string_value (accounts.active.secret);
		}

		if (is_comment) {
			builder.set_member_name ("comment_id");
			builder.add_int_value (comment_id);
		} else {
			builder.set_member_name ("post_id");
			builder.add_int_value (post_id);
		}

        builder.set_member_name ("score");
		builder.add_int_value (score);

		builder.end_object ();

        var msg = new Request.POST (@"https://$(accounts.active.instance)/api/v3/$(is_comment ?  "comment" : "post")/like").body_json (builder);
		if (!accounts.active.api_v18) {
			msg.with_account (accounts.active);
		}
		msg.await.begin ((o, res) => {
			try {
				msg.await.end (res);

				var parser = Network.get_parser_from_inputstream (msg.response_body);
				update_object (parser);

				debug ("Vote complete");
			} catch (Error e) {
				warning (@"Couldn't perform vote on a Post: $(e.message)");

				var dlg = app.inform (_("Network Error"), e.message);
				dlg.present ();

				vote_btn.active = !vote_btn.active;
			}

			vote_btn.unblock_clicked ();
		});
	}

	protected void copy_url () {
		Host.copy (action_url);
		app.toast (_("Copied url to clipboard"));
	}

	protected void open_in_browser () {
		Host.open_uri (action_url);
	}

	protected void save () {
		bool is_comment = comment_id != -1;

		var builder = new Json.Builder ();
		builder.begin_object ();

		if (accounts.active.api_v18) {
			builder.set_member_name ("auth");
			builder.add_string_value (accounts.active.secret);
		}

		if (is_comment) {
			builder.set_member_name ("comment_id");
			builder.add_int_value (comment_id);
		} else {
			builder.set_member_name ("post_id");
			builder.add_int_value (post_id);
		}

        builder.set_member_name ("save");
		builder.add_boolean_value (!is_saved);

		builder.end_object ();

        var msg = new Request.PUT (@"https://$(accounts.active.instance)/api/v3/$(is_comment ?  "comment" : "post")/save").body_json (builder);
		if (!accounts.active.api_v18) {
			msg.with_account (accounts.active);
		}
		msg.await.begin ((o, res) => {
			try {
				msg.await.end (res);

				var parser = Network.get_parser_from_inputstream (msg.response_body);
				update_object (parser);

				app.toast (is_saved ?_("Saved") : _("Unsaved"));
			} catch (Error e) {
				warning (@"Couldn't perform save: $(e.message)");

				var dlg = app.inform (_("Network Error"), e.message);
				dlg.present ();
			}
		});
	}

	private void update_object (Json.Parser parser) throws Error {
		if (comment_id != -1) {
			var comment_view = network.parse (parser).get_member ("comment_view");
			var wrapper = Trombone.API.Objects.Lemmy.CommentWrapper.from (comment_view);
			total_score = wrapper.counts.score;
			is_saved = wrapper.saved;
		} else {
			var post_view = network.parse (parser).get_member ("post_view");
			var wrapper = Trombone.API.Objects.Lemmy.PostWrapper.from (post_view);
			total_score = wrapper.counts.score;
			total_comments = wrapper.counts.comments;
			is_saved = wrapper.saved;
		}
	}
}
