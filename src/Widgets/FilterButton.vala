public class Trombone.Widgets.FilterButton : Adw.Bin {
    public enum ListFilter {
        HOT,
        ACTIVE,
        NEW,
        OLD,
        MOST_COMMENTS,
        NEW_COMMENTS,
        TOP_HOUR,
        TOP_SIX_HOURS,
        TOP_TWELVE_HOURS,
        TOP_DAY,
        TOP_WEEK,
        TOP_MONTH,
        TOP_THREE_MONTHS,
        TOP_SIX_MONTHS,
        TOP_NINE_MONTHS,
        TOP_YEAR,
        TOP_ALL;

        public string to_api () {
            switch (this) {
                case HOT: return "Hot";
                case ACTIVE: return "Active";
                case NEW: return "New";
                case OLD: return "Old";
                case MOST_COMMENTS: return "MostComments";
                case NEW_COMMENTS: return "NewComments";
                case TOP_HOUR: return "TopHour";
                case TOP_SIX_HOURS: return "TopSixHours";
                case TOP_TWELVE_HOURS: return "TopTwelveHours";
                case TOP_DAY: return "TopDay";
                case TOP_WEEK: return "TopWeek";
                case TOP_MONTH: return "TopMonth";
                case TOP_THREE_MONTHS: return "TopThreeMonths";
                case TOP_SIX_MONTHS: return "TopSixMonths";
                case TOP_NINE_MONTHS: return "TopNineMonths";
                case TOP_YEAR: return "TopYear";
                case TOP_ALL: return "TopAll";
                default: assert_not_reached ();
            }
        }

        public string to_string () {
            switch (this) {
                case HOT: return "Hot";
                case ACTIVE: return "Active";
                case NEW: return "New";
                case OLD: return "Old";
                case MOST_COMMENTS: return "Most Comments";
                case NEW_COMMENTS: return "New Comments";
                case TOP_HOUR: return "Top Hour";
                case TOP_SIX_HOURS: return "Top Six Hours";
                case TOP_TWELVE_HOURS: return "Top Twelve Hours";
                case TOP_DAY: return "Top Day";
                case TOP_WEEK: return "Top Week";
                case TOP_MONTH: return "Top Month";
                case TOP_THREE_MONTHS: return "Top Three Months";
                case TOP_SIX_MONTHS: return "Top Six Months";
                case TOP_NINE_MONTHS: return "Top Nine Months";
                case TOP_YEAR: return "Top Year";
                case TOP_ALL: return "Top All Time";
                default: assert_not_reached ();
            }
        }

        public static ListFilter? from_string (string list_type) {
            switch (list_type.up ()) {
                case "HOT": return HOT;
                case "ACTIVE": return ACTIVE;
                case "NEW": return NEW;
                case "OLD": return OLD;
                case "MOST COMMENTS": return MOST_COMMENTS;
                case "NEW COMMENTS": return NEW_COMMENTS;
                case "TOP HOUR": return TOP_HOUR;
                case "TOP SIX HOURS": return TOP_SIX_HOURS;
                case "TOP TWELVE HOURS": return TOP_TWELVE_HOURS;
                case "TOP DAY": return TOP_DAY;
                case "TOP WEEK": return TOP_WEEK;
                case "TOP MONTH": return TOP_MONTH;
                case "TOP THREE MONTHS": return TOP_THREE_MONTHS;
                case "TOP SIX MONTHS": return TOP_SIX_MONTHS;
                case "TOP NINE MONTHS": return TOP_NINE_MONTHS;
                case "TOP YEAR": return TOP_YEAR;
                case "TOP ALL TIME": return TOP_ALL;
                default: return null;
            }
        }
    }

    public signal void selected (ListFilter? list_filter);
    const string[] ALL_FILTERS = {"Hot", "Active", "New", "Old", "Most Comments", "New Comments", "Top Hour", "Top Six Hours", "Top Twelve Hours", "Top Day", "Top Week", "Top Month", "Top Three Months", "Top Six Months", "Top Nine Months", "Top Year", "Top All Time"};
    Gtk.DropDown dropdown;
    construct {
        dropdown = new Gtk.DropDown.from_strings (ALL_FILTERS) {
			tooltip_text = _("Filter"),
			enable_search = false,
            //  show_arrow = false
		};
        child = dropdown;
        dropdown.notify["selected"].connect (on_selection_update);
    }

    private string last_selection = "";
    private void on_selection_update () {
        string new_selection = ALL_FILTERS[dropdown.selected];
        if (new_selection == last_selection) return;
        last_selection = new_selection;

        selected (ListFilter.from_string (new_selection));
    }

    public bool set_selected_from_string (string new_filter) {
        ListFilter? list_filter = ListFilter.from_string (new_filter);
        if (list_filter == null) return false;
        return set_selected_from_list_filter (list_filter);
    }

    public bool set_selected_from_list_filter (ListFilter list_filter) {
        string list_filter_string = list_filter.to_string ();

        for (int i = 0; i < ALL_FILTERS.length; i++) {
            if (ALL_FILTERS[i] == list_filter_string) {
                dropdown.selected = i;
                return true;
            }
        }

        return false;
    }
}
