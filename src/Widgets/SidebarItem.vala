// The API might be a bit unconventional but it's made with ListView factories in mind
[GtkTemplate (ui = "/dev/geopjr/Trombone/windows/sidebar/item.ui")]
public class Trombone.Widgets.SidebarItem : Gtk.Box {
    public class Item : Object {
        [CCode (has_target = false)]
    	public delegate void OpenFunc ();

        public struct Basic {
            string title;
            string subtitle;
        }

        public API.Objects.Lemmy.Community? community { get; set; default=null; }
        public Basic? basic { get; set; default=null; }
        public bool separator { get; set; default=false; }
	    public OpenFunc? open_func { get; set; default=null; }
    }

    private bool is_community = false;
    private Widgets.Avatar? avi = null;
    private Gtk.Image? icon = null;

	[GtkChild] unowned Gtk.Label title;
	[GtkChild] unowned Gtk.Label subtitle;

    //  construct {
    //      this.add_css_class ("flat");
    //  }

    [GtkCallback] bool string_is_not_empty (string? text) {
        return text != null && text != "";
    }

    public bool separator {
        set {
            if (value) {
                this.add_css_class ("ttl-fake-separator");
            } else {
                this.remove_css_class ("ttl-fake-separator");
            }
        }
    }

    public void for_community (API.Objects.Lemmy.Community community) {
        if (icon != null) {
            this.remove (icon);
            icon = null;
        }

        is_community = true;
        this.title.label = community.title;
        this.subtitle.label = community.handle;

        if (avi == null) {
            avi = new Widgets.Avatar () {
                size = 28,
                can_focus = false,
                can_target = false
            };
            this.prepend (avi);
        }

        if (community.icon != null && community.icon != "") {
            avi.avatar_url = community.icon;
        } else {
            avi.fallback_text = community.title;
        }
    }

    public void for_basic (string t_title, string icon_name) {
        if (avi != null) {
            this.remove (avi);
            avi = null;
        }

        is_community = false;
        this.title.label = t_title;
        this.subtitle.label = "";

        if (icon == null) {
            icon = new Gtk.Image ();
            this.prepend (icon);
        }
        icon.icon_name = icon_name;
    }

    public void for_item (Item item) {
        if (item.community != null) {
			this.for_community (item.community);
		} else if (item.basic != null) {
			this.for_basic (item.basic.title, item.basic.subtitle);
        }

        //  if (item.separator) this.separator = true;
    }
}
