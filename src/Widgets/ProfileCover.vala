[GtkTemplate (ui = "/dev/geopjr/Trombone/widgets/profile_cover.ui")]
public class Trombone.Widgets.ProfileCover : Adw.Bin {
	[GtkChild] unowned Widgets.Avatar avatar;
	[GtkChild] unowned Widgets.Background background;
	[GtkChild] unowned Gtk.Label display_name;
	[GtkChild] unowned Gtk.Label handle;
	[GtkChild] unowned Gtk.Label note;
	[GtkChild] unowned Gtk.ListBox profile_list;

    void open_header_in_media_viewer () {
        if (header_url == null) return;
        app.main_window.show_media_viewer (header_url, Trombone.Attachment.MediaType.from_url (header_url), background.paintable, null, background, true);
    }

    void open_pfp_in_media_viewer () {
        if (avatar.avatar_url == null) return;
        app.main_window.show_media_viewer (avatar.avatar_url, Trombone.Attachment.MediaType.from_url (avatar.avatar_url), avatar.custom_image, null, avatar, true);
    }

    private string? header_url = null;
    public ProfileCover.from_person (API.Objects.Lemmy.Person person, API.Objects.Lemmy.Counts? counts = null) {
        display_name.label = person.public_name;
        handle.label = person.handle;

        if (person.banner == null) {
            header_url = null;
            background.paintable = avatar.custom_image;
        } else {
            header_url = person.banner;
            Trombone.Helper.Image.request_paintable (person.banner, null, null, on_cache_response);
            background.clicked.connect (open_header_in_media_viewer);
        }

        if (person.avatar == null) {
            avatar.fallback_text = person.public_name;
        } else {
            avatar.avatar_url = person.avatar;
            avatar.clicked.connect (open_pfp_in_media_viewer);
        }

        if (person.bio != null) note.label = person.bio;

        if (person.published != null) {
            var row = new Adw.ActionRow ();
            var parsed_date = new GLib.DateTime.from_iso8601 (@"$(person.published)Z", null);
            parsed_date = parsed_date.to_timezone (new TimeZone.local ());

            var date_local = _("%B %e, %Y");
            var val = new Gtk.Label (parsed_date.format (date_local).replace (" ", "")) { // %e prefixes with whitespace on single digits
                wrap = true,
                xalign = 1,
                hexpand = true,
                tooltip_text = parsed_date.format ("%F")
            };

            // translators: as in created an account; this is used in Profiles in a row
            //				which has as value the date the profile was created on
            row.title = _("Joined");

            row.add_suffix (val);
            row.add_prefix (new Gtk.Image.from_icon_name ("birthday-symbolic"));
            profile_list.append (row);
        }

        create_timeline_switcher ();

        if (counts != null) {
            update_stats_buttons (counts.post_count, counts.comment_count);
        }
    }

    Gtk.Button posts_button;
    Gtk.Button comments_button;
    void create_timeline_switcher () {
        var row = new Gtk.ListBoxRow ();
		var box = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
		var sizegroup = new Gtk.SizeGroup (Gtk.SizeGroupMode.HORIZONTAL);

		// translators: the variable is the amount of posts a user has made
		var btn = build_profile_stats_button (_("Overview"));
        btn.tooltip_text = _("Overview");
		//  btn.clicked.connect (() => timeline_change ("statuses"));
		sizegroup.add_widget (btn);
		box.append (btn);

		var separator = new Gtk.Separator (Gtk.Orientation.VERTICAL);
		box.append (separator);

		// translators: the variable is the amount of people a user follows
		posts_button = build_profile_stats_button (_("Posts"));
        posts_button.tooltip_text = _("Posts");
		//  btn.clicked.connect (() => timeline_change ("following"));
		sizegroup.add_widget (posts_button);
		box.append (posts_button);

		separator = new Gtk.Separator (Gtk.Orientation.VERTICAL);
		box.append (separator);

		// translators: the variable is the amount of followers a user has
		comments_button = build_profile_stats_button (_("Comments"));
        comments_button.tooltip_text = _("Comments");
		//  btn.clicked.connect (() => timeline_change ("followers"));
		sizegroup.add_widget (comments_button);
		box.append (comments_button);

		row.activatable = false;
		row.focusable = false;
		row.child = box;
		profile_list.append (row);
    }

    protected Gtk.Button build_profile_stats_button (string btn_label) {
		var btn = new Gtk.Button.with_label (btn_label) {
			css_classes = { "flat", "profile-stat-button" },
			hexpand = true
		};

		var child_label = btn.child as Gtk.Label;
		child_label.wrap = true;
		child_label.justify = Gtk.Justification.CENTER;

		return btn;
	}

    public void update_stats_buttons (int64 posts_count, int64 comments_count) {
        if (posts_count > 1) {
            posts_button.label = _("%s Posts").printf (Trombone.Units.shorten (posts_count));
            posts_button.tooltip_text = _("%s Posts").printf (posts_count.to_string ());
        }

        if (comments_count > 1) {
            comments_button.label = _("%s Comments").printf (Trombone.Units.shorten (comments_count));
            comments_button.tooltip_text = _("%s Comments").printf (comments_count.to_string ());
        }
    }

    void on_cache_response (Gdk.Paintable? data) {
        background.paintable = data;
    }
}
