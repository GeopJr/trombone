public class Trombone.API.InstanceAccount : Entity {
	public string name { get; set; }
	public string? avatar { get; set; }
	public string handle { get; set; }
	public string secret { get; set; }
	public string instance { get; set; }
	public string creation_date { get; set; }
	public string uuid { get; set; }
	public API.Backend.Type instance_type { get; set; }
	public API.Objects.Lemmy.SiteInfo? instance_info { get; set; default=null; }
	public bool api_v18 { get; set; default=false; }

	public InstanceAccount.from_person_and_token (API.Objects.Lemmy.Person person, string token, API.Backend.Type t_instance_type) {
		Object (
			secret: token,
			name: person.name,
			avatar: person.avatar,
			handle: person.handle,
			creation_date: person.published,
			instance_type: t_instance_type,
			uuid: GLib.Uuid.string_random ()
		);
	}

	public Json.Builder to_secret () {
		var builder = new Json.Builder ();
		builder.begin_object ();

        builder.set_member_name ("name");
		builder.add_string_value (name);

		if (avatar != null) {
			builder.set_member_name ("avatar");
			builder.add_string_value (avatar);
		}

		builder.set_member_name ("handle");
		builder.add_string_value (handle);

		builder.set_member_name ("secret");
		builder.add_string_value (secret);

		builder.set_member_name ("instance");
		builder.add_string_value (instance);

		builder.set_member_name ("uuid");
		builder.add_string_value (uuid);

		builder.set_member_name ("api_v18");
		builder.add_boolean_value (api_v18);

		builder.end_object ();

		return builder;
	}

	public static InstanceAccount from (Json.Node node) throws Error {
		return Entity.from_json (typeof (InstanceAccount), node) as InstanceAccount;
	}

	public async void verify_credentials () throws Error {
		if (instance_info != null) return;

		var req = new Request.GET ("/api/v3/site").with_account (this);
		yield req.await ();

		// check if the instance updated to > 18
		if (api_v18) {
			API.Backend.InstanceType guess = yield API.Backend.guess (instance);
			string[] version = guess.version.split (".");
			api_v18 = version.length >= 2 && int.parse (version[1]) <= 18;
		}

		update_object (req.response_body);
	}

	public void update_object (InputStream in_stream) throws Error {
		var parser = Network.get_parser_from_inputstream (in_stream);
		var node = network.parse_node (parser);
		instance_info = API.Objects.Lemmy.SiteInfo.from (node);
		var siteinfo_person = instance_info.my_user.local_user_view.person;

		this.name = siteinfo_person.name;
		this.avatar = siteinfo_person.avatar;
		this.handle = siteinfo_person.handle;
		this.creation_date = siteinfo_person.published;

		accounts.save (this);

		debug (@"$handle: profile updated");
	}

	// ???
	public virtual signal void activated () {
		if (accounts.active == this)
			app.update_communities ();
	}
	public virtual signal void deactivated () {}
	public virtual signal void added () {}
	public virtual signal void removed () {}
}
