public class Trombone.API.Backend : Object {
    public enum Type {
        LEMMY,
        KBIN,
        //  MBIN,
        UNKNOWN;

        public static Type from_nodeinfo (Trombone.API.Objects.Nodeinfo nodeinfo) {
            switch (nodeinfo.software.name.down ()) {
                case "lemmy":
                    return LEMMY;
                case "kbin":
                    return KBIN;
                //  case "mbin":
                //      return MBIN;
                default:
                    return UNKNOWN;
            }
        }

        public string to_string () {
            switch (this) {
                case LEMMY:
                    return "Lemmy";
                case KBIN:
                    return "/kbin";
                //  case MBIN:
                //      return "Mbin";
                default:
                    return "UNKNOWN";
            }
        }

        public bool is_supported () {
            return this == LEMMY;
        }
    }

    public struct InstanceType {
        public Type kind;
        public string version;
    }

    public static async InstanceType guess (string instance) throws Error {
        var req = new Request.GET (@"https://$instance/.well-known/nodeinfo");
        yield req.await ();

        var parser = Network.get_parser_from_inputstream (req.response_body);
        var node = network.parse_node (parser);
		var well_known_nodeinfo = Objects.WellKnown.Nodeinfo.from (node);

        if (well_known_nodeinfo.links.size == 0) return {Type.LEMMY, "0.18"};
        var nodeinfo_url = well_known_nodeinfo.links.get (0).href;

        req = new Request.GET (nodeinfo_url);
        yield req.await ();

        parser = Network.get_parser_from_inputstream (req.response_body);
        node = network.parse_node (parser);
	    var nodeinfo = API.Objects.Nodeinfo.from (node);

        return {Type.from_nodeinfo (nodeinfo), nodeinfo.software.version};
    }
}
