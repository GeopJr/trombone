public class Trombone.API.Objects.Lemmy.Person : Entity {
	public int64 id { get; set; }
	public string name { get; set; }
	public string? display_name { get; set; }
	public string? bio { get; set; }
	public string? avatar { get; set; }
	public string published { get; set; }
	public string actor_id { get; set; }
	public string? matrix_user_id { get; set; }
	public bool local { get; set; }
	public string? banner { get; set; }
	public bool deleted { get; set; }
	public bool admin { get; set; }
	public bool bot_account { get; set; }
	public bool banned { get; set; }

	public string handle {
		owned get {
			try {
				var uri = Uri.parse (actor_id, UriFlags.NONE);
				var host = uri.get_host ();
				//  if (host == null) throw Oopsie.INTERNAL ();

				return @"@$name@$host";
			} catch {
				var uri_parts = actor_id.split ("/");
				if (uri_parts.length < 3) return @"@$name";
				var host = uri_parts[2];
				return @"@$name@$host";
			}
		}
	}

	public string public_name {
		get {
			return display_name == null ? name : display_name;
		}
	}

    public static Person from (Json.Node node) throws Error {
		return Entity.from_json (typeof (Person), node) as Person;
	}

	public void open () {
		var dialog = new Adw.Window ();
		app.add_window (dialog);
		dialog.content = new Gtk.ScrolledWindow () {
			child = new Adw.Clamp () {
				child = new Widgets.ProfileCover.from_person (this)
			}
		};
		dialog.present ();
	}
}
