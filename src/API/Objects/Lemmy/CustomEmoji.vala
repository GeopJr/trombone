public class Trombone.API.Objects.Lemmy.CustomEmoji : Entity {
	public string category { get; set; }
	public string image_url { get; set; }
	public string alt_text { get; set; }
	public string shortcode { get; set; }

}

public class Trombone.API.Objects.Lemmy.CustomEmojiWrapper : Entity {
	public Trombone.API.Objects.Lemmy.CustomEmoji custom_emoji { get; set; }
	//  public Gee.ArrayList<string> keywords { get; set; }

    //  public override Type deserialize_array_type (string prop) {
	//  	switch (prop) {
	//  		case "keywords":
	//  			return Type.STRING;
	//  	}

	//  	return base.deserialize_array_type (prop);
	//  }
}
