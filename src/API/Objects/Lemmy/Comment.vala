public class Trombone.API.Objects.Lemmy.Comment : Entity {
	public int64 id { get; set; }
    public string content { get; set; }
    public string published { get; set; }
    public bool removed { get; set; }
    public bool deleted { get; set; }
    public string ap_id { get; set; }
}

public class Trombone.API.Objects.Lemmy.CommentWrapper : Entity {
    public class CommentCounts : Entity {
        public int64 score { get; set; }
        public int64 child_count { get; set; }
    }

	public CommentCounts counts { get; set; }
	public API.Objects.Lemmy.Comment comment { get; set; }
    public API.Objects.Lemmy.Person creator { get; set; }
    public API.Objects.Lemmy.Post post { get; set; }
    public API.Objects.Lemmy.Community community { get; set; }
    public string subscribed { get; set; }
    public bool saved { get; set; }
    public int64 my_vote { get; set; default=0; }

	public API.Objects.Lemmy.PostWrapper.VOTE voted {
		get {
			return API.Objects.Lemmy.PostWrapper.VOTE.from_int (my_vote);
		}
	}

    public static CommentWrapper from (Json.Node node) throws Error {
		return Entity.from_json (typeof (CommentWrapper), node) as CommentWrapper;
	}
}
