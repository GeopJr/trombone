public class Trombone.API.Objects.Lemmy.Language : Entity {
	public int64 id { get; set; }
	public string code { get; set; }
	public string name { get; set; }
}
