public class Trombone.API.Objects.Lemmy.Counts : Entity {
	public int64 post_count { get; set; }
	public int64 post_score { get; set; }
	public int64 comment_count { get; set; }
	public int64 comment_score { get; set; }

    public static Counts from (Json.Node node) throws Error {
		return Entity.from_json (typeof (Counts), node) as Counts;
	}
}
