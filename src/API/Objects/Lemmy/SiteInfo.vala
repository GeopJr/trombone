public class Trombone.API.Objects.Lemmy.SiteInfo : Entity {
	public class SiteView : Entity {
		public class LocalSite : Entity {
			public bool enable_downvotes { get; set; }
		}

		public LocalSite local_site { get; set; }
	}

	public class MyUser : Entity {
		public LocalUserView local_user_view { get; set; }
		public Gee.ArrayList<CommunityWrapper> follows { get; set; }

		public override Type deserialize_array_type (string prop) {
			switch (prop) {
				case "follows":
					return typeof (CommunityWrapper);
			}

			return base.deserialize_array_type (prop);
		}
	}

	public SiteView site_view { get; set; }
	public MyUser my_user { get; set; }
	public Gee.ArrayList<Language> all_languages { get; set; }
	public Gee.ArrayList<CustomEmojiWrapper> custom_emojis { get; set; }

	public override Type deserialize_array_type (string prop) {
		switch (prop) {
			case "all_languages":
				return typeof (Language);
			case "custom_emojis":
				return typeof (CustomEmojiWrapper);
		}

		return base.deserialize_array_type (prop);
	}

    public static SiteInfo from (Json.Node node) throws Error {
		return Entity.from_json (typeof (SiteInfo), node) as SiteInfo;
	}
}
