public class Trombone.API.Objects.Lemmy.LoginInfo : Entity {
	public string jwt { get; set; }

    public static LoginInfo from (Json.Node node) throws Error {
		return Entity.from_json (typeof (LoginInfo), node) as LoginInfo;
	}
}
