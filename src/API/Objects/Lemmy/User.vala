public class Trombone.API.Objects.Lemmy.User : Entity {
    public class PersonWrapper : Entity {
        public Person person { get; set; }
	    public Counts counts { get; set; }
    }

	public PersonWrapper person_view { get; set; }
	public Gee.ArrayList<API.Objects.Lemmy.PostWrapper> posts { get; set; }
	public Gee.ArrayList<API.Objects.Lemmy.CommentWrapper> comments { get; set; }

    public override Type deserialize_array_type (string prop) {
		switch (prop) {
			case "posts":
				return typeof (API.Objects.Lemmy.PostWrapper);
			case "comments":
				return typeof (API.Objects.Lemmy.CommentWrapper);
		}

		return base.deserialize_array_type (prop);
	}

    public static User from (Json.Node node) throws Error {
		return Entity.from_json (typeof (User), node) as User;
	}
}
