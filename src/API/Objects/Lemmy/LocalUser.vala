public class Trombone.API.Objects.Lemmy.LocalUser : Entity {
	public int64 id { get; set; }
	public int64 person_id { get; set; }
	public bool show_nsfw { get; set; }
	public string default_sort_type { get; set; }
	public string default_listing_type { get; set; }
	public bool show_scores { get; set; }

    public static LocalUser from (Json.Node node) throws Error {
		return Entity.from_json (typeof (LocalUser), node) as LocalUser;
	}
}
