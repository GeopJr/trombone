public class Trombone.API.Objects.Lemmy.LocalUserView : Entity {
	public LocalUser local_user { get; set; }
	public Person person { get; set; }
	public Counts counts { get; set; }

    public static LocalUserView from (Json.Node node) throws Error {
		return Entity.from_json (typeof (LocalUserView), node) as LocalUserView;
	}
}
