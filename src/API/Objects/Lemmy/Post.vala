public class Trombone.API.Objects.Lemmy.Post : Entity {
	public int64 id { get; set; }
	public string name { get; set; }
	public string? url { get; set; }
	public string? body { get; set; }
	public string published { get; set; }
	public bool locked { get; set; }
	public bool removed { get; set; }
	public bool nsfw { get; set; }
	public bool deleted { get; set; }
	public string thumbnail_url { get; set; }
	public string ap_id { get; set; }
	public bool local { get; set; }

    public static Post from (Json.Node node) throws Error {
		return Entity.from_json (typeof (Post), node) as Post;
	}
}

public class Trombone.API.Objects.Lemmy.PostWrapper : Entity {
    public class TempCount : Entity {
        public int score { get; set; }
        public int comments { get; set; }
    }

    public Post post { get; set; }
    public Person creator { get; set; }
    public Community community { get; set; }
    public bool creator_banned_from_community { get; set; }
    // TODO: counts??
    public TempCount counts { get; set; }
    public bool saved { get; set; }
    public bool read { get; set; }
    public bool creator_blocked { get; set; }
    public string subscribed { get; set; }
    public int64 my_vote { get; set; default=0; }

	public enum VOTE {
		UP,
		NONE,
		DOWN;

		public static VOTE from_int (int64 vote) {
			switch (vote) {
				case 1: return UP;
				case -1: return DOWN;
				default: return NONE;
			}
		}
	}

	public VOTE voted {
		get {
			return VOTE.from_int (my_vote);
		}
	}

    public static PostWrapper from (Json.Node node) throws Error {
		return Entity.from_json (typeof (PostWrapper), node) as PostWrapper;
	}
}

public class Trombone.API.Objects.Lemmy.PostWrapperParent : Entity {
    public Gee.ArrayList<PostWrapper> posts { get; set; }

    public override Type deserialize_array_type (string prop) {
		switch (prop) {
			case "posts":
				return typeof (PostWrapper);
		}

		return base.deserialize_array_type (prop);
	}

    public static PostWrapperParent from (Json.Node node) throws Error {
		return Entity.from_json (typeof (PostWrapperParent), node) as PostWrapperParent;
	}
}
