public class Trombone.API.Objects.Lemmy.Community : Entity {
	public int64 id { get; set; }
	public string name { get; set; }
	public string title { get; set; }
	public string description { get; set; }
	public string published { get; set; }
	public string actor_id { get; set; }
	public string icon { get; set; }
	public string banner { get; set; }
	public bool removed { get; set; }
	public bool deleted { get; set; }
	public bool nsfw { get; set; }
	public bool hidden { get; set; }
	public bool posting_restricted_to_mods { get; set; }
	public bool local { get; set; }

	public string handle {
		owned get {
			try {
				var uri = Uri.parse (actor_id, UriFlags.NONE);
				var host = uri.get_host ();
				//  if (host == null) throw Oopsie.INTERNAL ();

				return @"$name@$host";
			} catch {
				var uri_parts = actor_id.split ("/");
				if (uri_parts.length < 3) return name;
				var host = uri_parts[2];
				return @"$name@$host";
			}
		}
	}

	public static Community from (Json.Node node) throws Error {
		return Entity.from_json (typeof (Community), node) as Community;
	}
}

public class Trombone.API.Objects.Lemmy.CommunityWrapper : Entity {
	public Trombone.API.Objects.Lemmy.Community community { get; set; }
}

public class Trombone.API.Objects.Lemmy.CommunityExploreWrapper : Entity {
	public class CommunityExplore : Entity {
		public Trombone.API.Objects.Lemmy.Community community { get; set; }
		public API.Objects.Lemmy.CommunityCounts counts { get; set; }
		public string subscribed { get; set; }
	}

	public Gee.ArrayList<CommunityExplore> communities { get; set; }
	public override Type deserialize_array_type (string prop) {
		switch (prop) {
			case "communities":
				return typeof (CommunityExplore);
		}

		return base.deserialize_array_type (prop);
	}

	public static CommunityExploreWrapper from (Json.Node node) throws Error {
		return Entity.from_json (typeof (CommunityExploreWrapper), node) as CommunityExploreWrapper;
	}
}

public class Trombone.API.Objects.Lemmy.CommunityCounts : Entity {
	public int64 subscribers { get; set; }
	public int64 posts { get; set; }
}

public class Trombone.API.Objects.Lemmy.CommunityInfoWrapper : Entity {
	public class Moderator : Entity {
		public Person moderator { get; set; }
	}

	public class CommunityView : Entity {
		public API.Objects.Lemmy.Community community { get; set; }
		public string subscribed { get; set; }
		public API.Objects.Lemmy.CommunityCounts counts { get; set; }
	}

	public Gee.ArrayList<Moderator> moderators { get; set; }
	public CommunityView community_view { get; set; }

	public override Type deserialize_array_type (string prop) {
		switch (prop) {
			case "moderators":
				return typeof (Moderator);
		}

		return base.deserialize_array_type (prop);
	}

	public static CommunityInfoWrapper from (Json.Node node) throws Error {
		return Entity.from_json (typeof (CommunityInfoWrapper), node) as CommunityInfoWrapper;
	}
}
