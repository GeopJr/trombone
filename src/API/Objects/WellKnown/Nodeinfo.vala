public class Trombone.API.Objects.WellKnown.Nodeinfo : Entity {
	public class NodeinfoEntry : Entity {
		public string href { get; set; }
	}

	public Gee.ArrayList<NodeinfoEntry> links { get; set; }

	public override Type deserialize_array_type (string prop) {
		switch (prop) {
			case "links":
				return typeof (NodeinfoEntry);
		}

		return base.deserialize_array_type (prop);
	}

    public static Nodeinfo from (Json.Node node) throws Error {
		return Entity.from_json (typeof (Nodeinfo), node) as Nodeinfo;
	}
}
