public class Trombone.API.Objects.Nodeinfo : Entity {
	public class Software : Entity {
		public string name { get; set; }
		public string version { get; set; default=""; }
	}

	public Software software { get; set; }

    public static Nodeinfo from (Json.Node node) throws Error {
		return Entity.from_json (typeof (Nodeinfo), node) as Nodeinfo;
	}
}
