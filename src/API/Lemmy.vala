public class Trombone.API.Lemmy {
    public static async API.InstanceAccount login (string instance, string username_or_email, string password, string? totp = null, bool is_api_v18 = false) throws Error {
        var builder = new Json.Builder ();
		builder.begin_object ();

        builder.set_member_name ("username_or_email");
		builder.add_string_value (username_or_email);

        builder.set_member_name ("password");
		builder.add_string_value (password);

        if (totp != null && totp != "") {
            builder.set_member_name ("totp_2fa_token");
		    builder.add_string_value (totp);
        }

		builder.end_object ();

        var msg = new Request.POST (@"https://$instance/api/v3/user/login").body_json (builder);
        yield msg.await ();

        var parser = Network.get_parser_from_inputstream (msg.response_body);
        var node = network.parse_node (parser);
        var login_info = API.Objects.Lemmy.LoginInfo.from (node);

        msg = new Request.GET (@"https://$instance/api/v3/site");
        if (is_api_v18) {
            msg.with_param ("auth", login_info.jwt);
        } else {
			msg.with_token (login_info.jwt);
        }
		yield msg.await ();

        parser = Network.get_parser_from_inputstream (msg.response_body);
        node = network.parse_node (parser);

		var siteinfo_person = API.Objects.Lemmy.SiteInfo.from (node).my_user.local_user_view.person;
        return new API.InstanceAccount.from_person_and_token (siteinfo_person, login_info.jwt, API.Backend.Type.LEMMY) {
            instance = instance,
            api_v18 = is_api_v18
        };
    }
}
