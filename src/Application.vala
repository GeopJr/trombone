namespace Trombone {
    public errordomain Oopsie {
		USER,
		PARSING,
		INSTANCE,
		INTERNAL
	}

    public static Application app;

	public static AccountStore accounts;
	public static Settings settings;
	public static Network network;

    public static bool start_hidden = false;
	public static bool is_flatpak = false;
	public static string cache_path;
    public class Application : Adw.Application {
		public Windows.MainWindow? main_window { get; set; }
		public Windows.NewAccount? add_account_window { get; set; }

		// These are used for the GTK Inspector
		public Settings app_settings { get {return Trombone.settings; } }
		public AccountStore app_accounts { get {return Trombone.accounts; } }
		public Network app_network { get {return Trombone.network; } }

		public signal void update_communities ();
		public signal void refresh ();
		public signal void toast (string title, uint timeout = 5);

		private const GLib.ActionEntry[] APP_ENTRIES = {
			{ "about", on_about_action },
			{ "back", back_activated },
			{ "refresh", refresh_activated },
			//  { "search", search_activated },
			{ "quit", on_quit },
			{ "back-home", back_home_activated },
			//  { "scroll-page-down", scroll_view_page_down },
			//  { "scroll-page-up", scroll_view_page_up },
			//  { "open-status-url", open_status_url, "s" },
			//  { "answer-follow-request", answer_follow_request, "(ssb)" },
			//  { "follow-back", follow_back, "(ss)" },
			//  { "reply-to-status-uri", reply_to_status_uri, "(ss)" },
			//  { "remove-from-followers", remove_from_followers, "(ss)" },
			//  { "open-preferences", open_preferences },
			//  { "open-current-account-profile", open_current_account_profile }
		};

        construct {
            this.add_action_entries (APP_ENTRIES, this);
			this.set_accels_for_action ("app.about", {"F1"});
			//  this.set_accels_for_action ("app.open-preferences", {"<Ctrl>comma"});
			//  this.set_accels_for_action ("app.compose", {"<Ctrl>T", "<Ctrl>N"});
			this.set_accels_for_action ("app.back", {"<Alt>BackSpace", "<Alt>Left", "Escape", "<Alt>KP_Left", "Pointer_DfltBtnPrev"});
			this.set_accels_for_action ("app.refresh", {"<Ctrl>R", "F5"});
			//  this.set_accels_for_action ("app.search", {"<Ctrl>F"});
			this.set_accels_for_action ("app.quit", {"<Ctrl>Q"});
			this.set_accels_for_action ("window.close", {"<Ctrl>W"});
			this.set_accels_for_action ("app.back-home", {"<Alt>Home"});
			//  this.set_accels_for_action ("app.scroll-page-down", {"Page_Down"});
			//  this.set_accels_for_action ("app.scroll-page-up", {"Page_Up"});

			application_id = Build.DOMAIN;
			flags = ApplicationFlags.HANDLES_OPEN;
		}

		void on_quit () {
			this.quit ();
		}

		void back_activated () {
			main_window.back ();
		}

		void refresh_activated () {
			refresh ();
		}

		void back_home_activated () {
			main_window.go_back_to_start ();
		}

        protected override void startup () {
			base.startup ();
			try {
				Adw.init ();

				settings = new Settings ();
				network = new Network ();
				accounts = new SecretAccountStore ();
				accounts.init ();
			} catch (Error e) {
				var msg = "Could not start application: %s".printf (e.message);

				error (msg);
			}

			var style_manager = Adw.StyleManager.get_default ();
			ColorScheme color_scheme = (ColorScheme) settings.get_enum ("color-scheme");
			style_manager.color_scheme = color_scheme.to_adwaita_scheme ();
		}

		public static int main (string[] args) {
			//  try {
			//  	var opt_context = new OptionContext ("- Options");
			//  	opt_context.add_main_entries (APP_OPTIONS, null);
			//  	opt_context.parse (ref args);
			//  } catch (GLib.OptionError e) {
			//  	warning (e.message);
			//  }

			cache_path = GLib.Path.build_path (GLib.Path.DIR_SEPARATOR_S, GLib.Environment.get_user_cache_dir (), Build.NAME.down ());

			is_flatpak = GLib.Environment.get_variable ("FLATPAK_ID") != null || GLib.File.new_for_path ("/.flatpak-info").query_exists ();

			Intl.setlocale (LocaleCategory.ALL, "");
			Intl.bindtextdomain (Build.GETTEXT_PACKAGE, Build.LOCALEDIR);
			Intl.textdomain (Build.GETTEXT_PACKAGE);

			GLib.Environment.unset_variable ("GTK_THEME");

			app = new Application ();
			return app.run (args);
		}

		private bool activated = false;
        public override void activate () {
			activated = true;
			present_window ();

			if (start_hidden) {
				start_hidden = false;
				return;
			}
			settings.delay ();
            //  base.activate ();
        }

		protected override void shutdown () {
			settings.apply_all ();
			network.flush_cache ();

			base.shutdown ();
		}

        private void on_about_action () {
            string[] developers = { "GeopJr" };
            var about = new Adw.AboutWindow () {
                transient_for = this.active_window,
                application_name = "trombone",
                application_icon = "dev.geopjr.Trombone",
                developer_name = "GeopJr",
                version = "0.1.0",
                developers = developers,
                copyright = "© 2023 GeopJr",
            };

            about.present ();
        }

		public void present_window (bool destroy_main = false) {
			if (accounts.saved.is_empty) {
				if (main_window != null && destroy_main)
					main_window.hide ();
				debug ("Presenting NewAccount dialog");
				if (add_account_window == null)
					new Trombone.Windows.NewAccount ();
				add_account_window.present ();
			} else {
				debug ("Presenting MainWindow");
				if (main_window == null) {
					main_window = new Trombone.Windows.MainWindow (this);
				}
				if (!start_hidden) main_window.present ();
			}

			if (main_window != null)
				main_window.close_request.connect (on_window_closed);
		}

		public bool on_window_closed () {
			if (!settings.work_in_background || accounts.saved.is_empty) {
				main_window.hide_on_close = false;
			} else {
				main_window.hide_on_close = true;
			}

			return false;
		}

        private void on_preferences_action () {
            message ("app.preferences action activated");
        }

		public Adw.MessageDialog inform (string text, string? msg = null, Gtk.Window? win = app.main_window) {
			var dlg = new Adw.MessageDialog (
				win,
				text,
				msg
			);

			if (win != null)
				dlg.transient_for = win;

			dlg.add_response ("ok", _("OK"));

			return dlg;
		}

		public struct QuestionButton {
			public string label;
			public Adw.ResponseAppearance appearance;
		}

		public struct QuestionButtons {
			public QuestionButton yes;
			public QuestionButton no;
		}

		public struct QuestionText {
			public string text;
			public bool use_markup;
		}

		public enum QuestionAnswer {
			YES,
			NO,
			CLOSE;

			public static QuestionAnswer from_string (string answer) {
				switch (answer.down ()) {
					case "yes":
						return YES;
					case "no":
						return NO;
					default:
						return CLOSE;
				}
			}

			public bool truthy () {
				return this == YES;
			}

			public bool falsy () {
				return this != YES;
			}
		}

		public async QuestionAnswer question (
			QuestionText title,
			QuestionText? msg = null,
			Gtk.Window? win = null,
			QuestionButtons buttons = {
				{ _("Yes"), Adw.ResponseAppearance.DEFAULT },
				{ _("Cancel"), Adw.ResponseAppearance.DEFAULT }
			},
			bool skip = false // skip the dialog, used for preferences to avoid duplicate code
		) {
			if (skip) return QuestionAnswer.YES;

			var dlg = new Adw.MessageDialog (
				win,
				title.text,
				msg == null ? null : msg.text
			);

			dlg.heading_use_markup = title.use_markup;
			if (msg != null) dlg.body_use_markup = msg.use_markup;

			dlg.add_response ("no", buttons.no.label);
			dlg.set_response_appearance ("no", buttons.no.appearance);

			dlg.add_response ("yes", buttons.yes.label);
			dlg.set_response_appearance ("yes", buttons.yes.appearance);

			if (win != null)
				dlg.transient_for = win;
			return QuestionAnswer.from_string (yield dlg.choose (null));
		}

	}

	public static void toggle_css (Gtk.Widget wdg, bool state, string style) {
		if (state) {
			wdg.add_css_class (style);
		} else {
			wdg.remove_css_class (style);
		}
	}
}
