public class Trombone.Views.Profile : Views.Timeline, AccountHolder {
    public class PersonWithStats : Entity {
        public API.Objects.Lemmy.Person person { get; set; }
        public API.Objects.Lemmy.Counts? counts { get; set; default=null; }

        public PersonWithStats (API.Objects.Lemmy.Person t_person, API.Objects.Lemmy.Counts? t_counts = null) {
            this.person = t_person;
            this.counts = t_counts;
        }

        public bool equals (PersonWithStats other) {
            if ((counts != null && other.counts == null) || (counts == null && other.counts != null)) return false;
            bool person_equal = person.public_name == other.person.public_name
                && person.banner == other.person.banner
                && person.avatar == other.person.avatar
                && person.bio == other.person.bio;

            bool counts_equal = true;
            if (counts != null) {
                counts_equal = counts.post_count == other.counts.post_count && counts.comment_count == other.counts.comment_count;
            }

            return person_equal && counts_equal;
        }
    }

    string profile_handle;
    PersonWithStats cover_stats;
    public Profile (API.Objects.Lemmy.Person person) {
        Object (
            list_type: ListType.NONE,
            title: _("Profile"),
            tag: @"profile-$(person.handle)",
            api_url: "/api/v3/user",
            sort_type: Widgets.FilterButton.ListFilter.NEW
        );
        profile_handle = person.handle.substring (1);

        //  content.remove_css_class ("content");
        cover_stats = new PersonWithStats (person);
        model.append (cover_stats);
    }

    protected override void setup_listitem_cb (GLib.Object item) {
        ((Gtk.ListItem) item).child = new Widgets.Post ();
	}

	protected override void bind_listitem_cb (GLib.Object item) {
        var cover = ((Entity) ((Gtk.ListItem) item).item) as PersonWithStats;
        if (cover != null) {
            ((Gtk.ListItem) item).child = new Widgets.ProfileCover.from_person (cover.person, cover.counts);
        } else {
    		((Gtk.ListItem) item).selectable = true;

            var post = ((Entity) ((Gtk.ListItem) item).item) as API.Objects.Lemmy.PostWrapper;
		    var post_widget = (Widgets.Post) ((Gtk.ListItem) item).child;
            if (post != null) {
                post_widget.from_post_wrapper (post, false);
            } else {
                post_widget.from_comment_wrapper ((API.Objects.Lemmy.CommentWrapper) ((Gtk.ListItem) item).item);
            }
        }

        var gtklistitemwidget = ((Gtk.Widget) ((Gtk.ListItem) item).child).get_parent ();
		if (gtklistitemwidget != null) {
			gtklistitemwidget.add_css_class ("card");
			gtklistitemwidget.add_css_class ("card-spacing");
            if (cover != null) gtklistitemwidget.overflow = Gtk.Overflow.HIDDEN;
			//  gtklistitemwidget.focusable = true;
		}
	}

    protected override bool refresh_t () {
        working = true;

        new Request.GET (api_url).with_account (account)
            .with_param ("page", page.to_string ())
            .with_param ("limit", limit.to_string ())
            .with_param ("sort", sort_type.to_api ())
            .with_param ("username", profile_handle)
            .then ((in_stream) => {
                var parser = Network.get_parser_from_inputstream (in_stream);
                var node = network.parse_node (parser);
                var wrapper = Trombone.API.Objects.Lemmy.User.from (node);
                if (wrapper.posts.size == 0 && wrapper.comments.size == 0) {
                    last_page = true;
                    return;
                }

                PersonWithStats wrapper_profile = new PersonWithStats (wrapper.person_view.person, wrapper.person_view.counts);
                if (model.n_items == 0 || !cover_stats.equals (wrapper_profile)) {
                    if (model.n_items > 0) model.remove (0);
                    model.insert (0, wrapper_profile);
                    cover_stats = wrapper_profile;
                }

                Gee.ArrayList<Entity> all_posts = wrapper.comments;
                all_posts.add_all ((Gee.ArrayList<Entity>) wrapper.posts);
                all_posts.sort (new_compare_func);
                model.splice (model.n_items, 0, all_posts.to_array ());
                working = false;
            })
            .on_error (() => {
                working = false;
            })
            .exec ();

        return GLib.Source.REMOVE;
    }

    protected int new_compare_func (Entity a, Entity b) {
        var entity_a = datetime_from_entity (a);
        var entity_b = datetime_from_entity (b);

        if (entity_a == null && entity_b == null) return 0;
        if (entity_a == null) return 1;
        if (entity_b == null) return -1;

        return entity_b.compare (entity_a);
    }

    private GLib.DateTime? datetime_from_entity (Entity entity) {
        GLib.DateTime? res = null;
        var t_entity = entity as API.Objects.Lemmy.CommentWrapper;
        if (t_entity != null) {
            if (t_entity.comment.published == null) return null;
            res = new GLib.DateTime.from_iso8601 (@"$(t_entity.comment.published)Z", null);
        } else {
            var t_post = (API.Objects.Lemmy.PostWrapper) entity;
            if (t_post.post.published == null) return null;
            res = new GLib.DateTime.from_iso8601 (@"$(t_post.post.published)Z", null);
        }

        return res;
    }
}
