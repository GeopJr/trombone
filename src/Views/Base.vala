[GtkTemplate (ui = "/dev/geopjr/Trombone/views/base.ui")]
public class Trombone.Views.Base : Adw.NavigationPage {
	[GtkChild] protected unowned Gtk.ScrolledWindow scroller;
	[GtkChild] unowned Gtk.Overlay scroller_overlay;
	//  [GtkChild] unowned Gtk.Box status;
	[GtkChild] unowned Gtk.Stack main_stack;
	[GtkChild] unowned Gtk.Stack status_stack;
	[GtkChild] unowned Gtk.Label status_title_label;
	[GtkChild] unowned Gtk.Label status_message_label;
	[GtkChild] unowned Gtk.Spinner status_spinner;
	[GtkChild] unowned Gtk.Button status_button;
	[GtkChild] protected unowned Adw.ToolbarView toolabr_view;

    public bool is_sidebar_item { get; construct set; default=false; }
    public class StatusMessage : Object {
		public string title = "";
		public string? message = null;
		public bool loading = false;
	}

	private StatusMessage? _base_status = null;
	public StatusMessage? base_status {
		get {
			return _base_status;
		}
		set {
			if (value == null) {
				main_stack.visible_child_name = "content";
				status_spinner.spinning = false;
			} else {
				main_stack.visible_child_name = "status";
				if (value.loading) {
					status_stack.visible_child_name = "spinner";
					status_spinner.spinning = true;
				} else {
					status_stack.visible_child_name = "message";
					status_spinner.spinning = false;

					status_title_label.label = value.title;
					if (value.message != null)
						status_message_label.label = value.message;
				}
			}
			_base_status = value;
		}
	}

    public signal void pulled ();
    private Gtk.Spinner pull_to_refresh_spinner;
	private bool _is_pulling = false;
	private bool is_pulling {
		get {
			return _is_pulling;
		}
		set {
			if (_is_pulling != value) {
				if (value) {
					pull_to_refresh_spinner.spinning = true;
					scroller_overlay.add_overlay (pull_to_refresh_spinner);
					scroller.sensitive = false;
				} else {
					pull_to_refresh_spinner.spinning = false;
					scroller_overlay.remove_overlay (pull_to_refresh_spinner);
					scroller.sensitive = true;
					pull_to_refresh_spinner.margin_top = 32;
					pull_to_refresh_spinner.height_request = 32;
				}
				_is_pulling = value;
			}
		}
	}

    private void on_drag_update (double x, double y) {
		if (scroller.vadjustment.value != 0.0 || (y <= 0 && !is_pulling)) return;
		is_pulling = true;

		double clean_y = y;
		if (y > 150) {
			clean_y = 150;
		} else if (y < -32) {
			clean_y = -32;
		}

		if (clean_y > 32) {
			pull_to_refresh_spinner.margin_top = (int) clean_y;
			pull_to_refresh_spinner.height_request = pull_to_refresh_spinner.width_request = 32;
		} else if (clean_y > 0) {
			pull_to_refresh_spinner.height_request = pull_to_refresh_spinner.width_request = (int) clean_y;
		} else {
			pull_to_refresh_spinner.margin_top = 32;
			pull_to_refresh_spinner.height_request = pull_to_refresh_spinner.width_request = 0;
		}
    }

    private void on_drag_end (double x, double y) {
        if (scroller.vadjustment.value == 0.0 && pull_to_refresh_spinner.margin_top >= 125) {
            pulled ();
        }

		is_pulling = false;
    }

    protected virtual Adw.HeaderBar construct_headerbar () {
        return new Adw.HeaderBar ();
    }

    construct {
        pull_to_refresh_spinner = new Gtk.Spinner () {
			height_request = 32,
			width_request = 32,
			margin_top = 32,
			margin_bottom = 32,
			halign = Gtk.Align.CENTER,
			valign = Gtk.Align.START,
			css_classes = { "osd", "circular-spinner" }
		};

        var drag = new Gtk.GestureDrag ();
        drag.drag_update.connect (on_drag_update);
        drag.drag_end.connect (on_drag_end);
        this.add_controller (drag);

        status_button.clicked.connect (status_button_clicked);
    }

    void status_button_clicked () {
        pulled ();
    }

    protected virtual void on_error (int32 code, string reason) {
		base_status = new StatusMessage () {
			title = _("An Error Occurred"),
			message = reason
		};

		status_button.visible = true;
		status_button.sensitive = true;
	}
}
