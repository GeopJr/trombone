public class Trombone.Views.Saved : Views.Timeline, AccountHolder {
    Gtk.DropDown dropdown;
    const string[] ALL_TYPES = {"Posts", "Comments"};

    public enum FilterType {
        POSTS,
        COMMENTS;

        public static FilterType from_string (string filter_name) {
            switch (filter_name.up ()) {
                case "COMMENTS": return COMMENTS;
                default: return POSTS;
            }
        }
    }
    FilterType filter_type = FilterType.POSTS;

    construct {
        this.title = _("Saved");
        this.tag = "saved";
        this.sort_type = Widgets.FilterButton.ListFilter.NEW;
        this.api_url = "/api/v3/user?saved_only=true";

		no_selection.notify["selected"].connect (on_content_item_activated);
    }

    protected override Adw.HeaderBar construct_headerbar () {
        var headerbar = new Adw.HeaderBar ();

        dropdown = new Gtk.DropDown.from_strings ({"Posts", "Comments"}) {
			tooltip_text = _("Filter"),
			enable_search = false,
            //  show_arrow = false
		};
        dropdown.notify["selected"].connect (on_selection_update);
        headerbar.pack_end (dropdown);

        return headerbar;
    }

    private void on_content_item_activated () {
		//  var selected_item = (API.Objects.Lemmy.CommunityExploreWrapper.CommunityExplore) no_selection.selected_item;
        //  app.main_window.open_view (new Views.Community (selected_item.community));
    }

    private string last_selection = "";
    private void on_selection_update () {
        string new_selection = ALL_TYPES[dropdown.selected];
        if (new_selection == last_selection) return;
        last_selection = new_selection;
        var new_type = FilterType.from_string (new_selection);
        filter_type = new_type;
        clear ();
    }

    protected override bool refresh_t () {
        working = true;

        new Request.GET (api_url).with_account (account)
            .with_param ("page", page.to_string ())
            .with_param ("limit", limit.to_string ())
            .with_param ("sort", sort_type.to_api ())
            .with_param ("username", account.name)
            .then ((in_stream) => {
                bool posts_only = filter_type == FilterType.POSTS;
                var parser = Network.get_parser_from_inputstream (in_stream);
                var node = network.parse_node (parser);
                var wrapper = Trombone.API.Objects.Lemmy.User.from (node);
                if ((posts_only && wrapper.posts.size == 0) || (!posts_only && wrapper.comments.size == 0)) {
                    last_page = true;
                    return;
                }

                Entity[] to_add = {};
                // Union types, what a concept!
                if (posts_only) {
                    foreach (var entity in wrapper.posts) {
                        to_add += entity;
                    }
                } else {
                    foreach (var entity in wrapper.comments) {
                        to_add += entity;
                    }
                }

                model.splice (model.n_items, 0, to_add);
                working = false;
            })
            .on_error (() => {
                working = false;
            })
            .exec ();

        return GLib.Source.REMOVE;
    }

    protected override void setup_listitem_cb (GLib.Object item) {
        ((Gtk.ListItem) item).child = new Widgets.Post ();
	}

	protected override void bind_listitem_cb (GLib.Object item) {
		((Gtk.ListItem) item).selectable = true;
		var post_widget = (Widgets.Post) ((Gtk.ListItem) item).child;

        var gtklistitemwidget = post_widget.get_parent ();
		if (gtklistitemwidget != null) {
			gtklistitemwidget.add_css_class ("card");
			gtklistitemwidget.add_css_class ("card-spacing");
			//  gtklistitemwidget.focusable = true;
		}

        if (filter_type == FilterType.POSTS) {
            post_widget.from_post_wrapper ((API.Objects.Lemmy.PostWrapper) ((Gtk.ListItem) item).item, community_name != "");
        } else {
            post_widget.from_comment_wrapper ((API.Objects.Lemmy.CommentWrapper) ((Gtk.ListItem) item).item);
        }
	}
}
