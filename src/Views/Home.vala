public class Trombone.Views.Home : Views.Timeline {
    construct {
        this.title = _("Home");
        this.tag = "home";
        this.api_url = "/api/v3/post/list";
        this.list_type = ListType.SUBSCRIBED;
    }

    private Gtk.Button sidebar_button;
    public signal void sidebar_toggled ();
    protected override Adw.HeaderBar construct_headerbar () {
        var headerbar = base.construct_headerbar ();
        headerbar.show_title = false;

        sidebar_button = new Gtk.Button.from_icon_name ("dock-left-symbolic");
        sidebar_button.clicked.connect (on_sidebar_button_clicked);
		headerbar.pack_start (sidebar_button);

        return headerbar;
    }

    private void on_sidebar_button_clicked () {
        sidebar_toggled ();
    }

    public bool sidebar_collapsed {
        set {
            sidebar_button.visible = value;
        }
    }
}
