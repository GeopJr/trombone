public class Trombone.Views.Timeline : Views.Base, AccountHolder {
    protected Gtk.ListView content;
	protected GLib.ListStore model;
	protected Gtk.SingleSelection no_selection;

    protected bool working = false;
    protected bool last_page = false;

    protected override Adw.HeaderBar construct_headerbar () {
        var headerbar = base.construct_headerbar ();
        var filterbtn = new Trombone.Widgets.FilterButton ();
        filterbtn.set_selected_from_list_filter (sort_type);
        filterbtn.selected.connect (on_filter_update);
        headerbar.pack_end (filterbtn);

        return headerbar;
    }

    public Type list_view_type { get; construct set; default=typeof (Entity); }
    construct {
        base_status = new StatusMessage () { loading = true };
        app.refresh.connect (clear);
        this.add_css_class ("ttl-view");
        model = new GLib.ListStore (list_view_type);
        model.items_changed.connect (on_content_changed);
		Gtk.SignalListItemFactory signallistitemfactory = new Gtk.SignalListItemFactory ();
		signallistitemfactory.setup.connect (setup_listitem_cb);
		signallistitemfactory.bind.connect (bind_listitem_cb);

		no_selection = new Gtk.SingleSelection (model) {
            autoselect = false
        };
		content = new Gtk.ListView (no_selection, signallistitemfactory) {
			css_classes = { "content" },
			single_click_activate = false
		};

        toolabr_view.add_top_bar (construct_headerbar ());
        scroller.child = new Adw.ClampScrollable () {
                child = content,
                maximum_size = 670,
                tightening_threshold = 670
        };
        scroller.vadjustment.value_changed.connect (on_scrolled_vadjustment_value_change);
        pulled.connect (clear);

        construct_account_holder ();
    }

    protected void on_filter_update (Widgets.FilterButton.ListFilter? list_filter) {
        if (list_filter == null) return;
        sort_type = list_filter;
        clear ();
    }

    protected virtual void setup_listitem_cb (GLib.Object item) {
		((Gtk.ListItem) item).child = new Widgets.Post ();
	}

	protected virtual void bind_listitem_cb (GLib.Object item) {
		((Gtk.ListItem) item).selectable = true;
		var sidebar_child = (Widgets.Post) ((Gtk.ListItem) item).child;
		//  var sidebar_item = (API.Objects.Lemmy.PostWrapper) ((Gtk.ListItem) item).item;

        var gtklistitemwidget = sidebar_child.get_parent ();
		if (gtklistitemwidget != null) {
			gtklistitemwidget.add_css_class ("card");
			gtklistitemwidget.add_css_class ("card-spacing");
			//  gtklistitemwidget.focusable = true;
		}

		sidebar_child.from_post_wrapper ((API.Objects.Lemmy.PostWrapper) ((Gtk.ListItem) item).item, community_name != "");
	}


    public enum ListType {
        ALL,
        LOCAL,
        SUBSCRIBED,
        NONE;

        public string to_string () {
            switch (this) {
                case ALL: return "All";
                case LOCAL: return "Local";
                case SUBSCRIBED: return "Subscribed";
                default: return "";
            }
        }

        public static ListType? from_string (string list_type) {
            switch (list_type.up ()) {
                case "ALL": return ALL;
                case "LOCAL": return LOCAL;
                case "SUBSCRIBED": return SUBSCRIBED;
                default: return null;
            }
        }
    }

    protected string? api_url { get; set; }
    protected virtual bool refresh_t () {
        if (api_url == null) return GLib.Source.REMOVE;
        working = true;

        var req = new Request.GET (api_url).with_account (account)
            .with_param ("page", page.to_string ())
            .with_param ("limit", limit.to_string ())
            .with_param ("sort", sort_type.to_api ());

        if (community_name != "") {
            req = req
                .with_param ("community_name", community_name)
                .with_param ("type_", ListType.ALL.to_string ());
        } else if (list_type != ListType.NONE) {
            req = req.with_param ("type_", list_type.to_string ());
        }

        req
            .then ((in_stream) => {
                var parser = Network.get_parser_from_inputstream (in_stream);
                var node = network.parse_node (parser);
                var wrapper = Trombone.API.Objects.Lemmy.PostWrapperParent.from (node);
                if (wrapper.posts.size == 0) {
                    last_page = true;
                    return;
                }

                API.Objects.Lemmy.PostWrapper[] to_add = {};
                foreach (var postwrapper in wrapper.posts) {
                    to_add += postwrapper;
                }
                model.splice (model.n_items, 0, to_add);
                working = false;
            })
            .on_error (() => {
                working = false;
            })
            .exec ();

        return GLib.Source.REMOVE;
    }

    public virtual void on_content_changed () {
		if (model.n_items <= 0) {
			base_status = new StatusMessage () { loading = true };
		} else {
			base_status = null;
		}
	}

    protected virtual void clear () {
        page = 1;
        model.remove_all ();
        GLib.Idle.add (refresh_t);
    }

    protected virtual void on_scrolled_vadjustment_value_change () {
		if (
			!working
			&& scroller.vadjustment.value > scroller.vadjustment.upper - scroller.vadjustment.page_size * 2
		) {
			on_bottom_reached ();
		}
	}

    protected virtual void on_bottom_reached () {
        if (last_page) return;
        working = true;
        page += 1;
        GLib.Idle.add (refresh_t);
    }

    public string community_name { get; set; default=""; }
    public int page { get; set; default=1; }
    public int limit { get; set; default=20; }
    public ListType list_type { get; construct set; }
    public Widgets.FilterButton.ListFilter sort_type { get; set; default=Widgets.FilterButton.ListFilter.ACTIVE; }
	protected API.InstanceAccount? account { get; set; default = null; }
    protected virtual void on_account_changed (API.InstanceAccount? account) {
        this.account = account;
		if (account != null) {
            clear ();
		}
    }
}
