public class Trombone.Views.Community : Views.Timeline {
    construct {
        this.api_url = "/api/v3/post/list";
        this.list_type = ListType.ALL;
    }

    Adw.WindowTitle headerbar_title;
    protected override Adw.HeaderBar construct_headerbar () {
        var headerbar = base.construct_headerbar ();

        info_btn = new Gtk.Button.from_icon_name ("info-symbolic") {
            visible = false,
            css_classes = { "flat" }
        };
        headerbar.pack_start (info_btn);
        info_btn.clicked.connect (show_community_info);

        joinbtn = new Widgets.JoinActionButton () {
            visible = false
        };
        headerbar.pack_end (joinbtn);

        headerbar_title = new Adw.WindowTitle (this.title, this.tag);
        headerbar.title_widget = headerbar_title;

        return headerbar;
    }

    private API.Objects.Lemmy.CommunityInfoWrapper? community_info { get; set; }
    private Widgets.JoinActionButton joinbtn;
    private Gtk.Button info_btn;
    public Community (API.Objects.Lemmy.Community community) {
        Object (
            title: community.title,
            tag: community.handle,
            community_name: community.handle
        );

        headerbar_title.title = community.title;
        headerbar_title.subtitle = community.handle;
        joinbtn.community_id = community.id;
        GLib.Idle.add (gather_community_info);
    }

    private bool gather_community_info () {
        new Request.GET ("/api/v3/community").with_account (account)
            .with_param ("name", this.community_name)
            .then ((in_stream) => {
                var parser = Network.get_parser_from_inputstream (in_stream);
                var node = network.parse_node (parser);
                community_info = API.Objects.Lemmy.CommunityInfoWrapper.from (node);
                info_btn.visible = true;
                joinbtn.visible = true;
		        joinbtn.subscription = Widgets.JoinActionButton.SUBSCRIPTION.from_string (community_info.community_view.subscribed);
            })
            .exec ();

        return GLib.Source.REMOVE;
    }

    private void show_community_info () {
        if (community_info == null) return;
        new Windows.CommunityInfo (community_info);
    }
}
