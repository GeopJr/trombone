public class Trombone.Views.Local : Views.Timeline {
    construct {
        this.api_url = "/api/v3/post/list";
        list_type = ListType.LOCAL;
        this.title = _("Local");
        this.tag = "local";
    }
}
