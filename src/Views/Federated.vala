public class Trombone.Views.Federated : Views.Timeline {
    construct {
        this.title = _("Federated");
        this.tag = "federated";
        this.api_url = "/api/v3/post/list";
        this.list_type = ListType.ALL;
    }
}
