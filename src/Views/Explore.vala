public class Trombone.Views.Explore : Views.Timeline, AccountHolder {
    Gtk.DropDown dropdown;
    const string[] ALL_TYPES = {"All", "Local"};

    construct {
        this.title = _("Explore");
        this.tag = "explore";
        this.api_url = "/api/v3/community/list";
        this.list_type = ListType.ALL;

		no_selection.notify["selected"].connect (on_content_item_activated);
    }

    protected override Adw.HeaderBar construct_headerbar () {
        var headerbar = new Adw.HeaderBar ();

        dropdown = new Gtk.DropDown.from_strings ({"All", "Local"}) {
			tooltip_text = _("Federation"),
			enable_search = false,
            //  show_arrow = false
		};
        dropdown.notify["selected"].connect (on_selection_update);
        headerbar.pack_end (dropdown);

        return headerbar;
    }

    private void on_content_item_activated () {
		var selected_item = (API.Objects.Lemmy.CommunityExploreWrapper.CommunityExplore) no_selection.selected_item;
        app.main_window.open_view (new Views.Community (selected_item.community));
    }

    private string last_selection = "";
    private void on_selection_update () {
        string new_selection = ALL_TYPES[dropdown.selected];
        if (new_selection == last_selection) return;

        var new_type = Views.Timeline.ListType.from_string (new_selection);
        if (new_type == null) return;
        last_selection = new_selection;

        list_type = new_type;
        clear ();
    }

    protected override bool refresh_t () {
        working = true;

        new Request.GET ("/api/v3/community/list").with_account (account)
            .with_param ("page", page.to_string ())
            .with_param ("limit", "50")
            .with_param ("sort", "TopMonth")
            .with_param ("type_", list_type.to_string ())
            .then ((in_stream) => {
                var parser = Network.get_parser_from_inputstream (in_stream);
                var node = network.parse_node (parser);
                var wrapper = Trombone.API.Objects.Lemmy.CommunityExploreWrapper.from (node);
                if (wrapper.communities.size == 0) {
                    last_page = true;
                    return;
                }

                API.Objects.Lemmy.CommunityExploreWrapper.CommunityExplore[] to_add = {};
                foreach (var community_wrapper in wrapper.communities) {
                    to_add += community_wrapper;
                }
                model.splice (model.n_items, 0, to_add);
                working = false;
            })
            .on_error (() => {
                working = false;
            })
            .exec ();

        return GLib.Source.REMOVE;
    }

    protected override void setup_listitem_cb (GLib.Object item) {
		((Gtk.ListItem) item).child = new Widgets.CommunityRow ();
	}

	protected override void bind_listitem_cb (GLib.Object item) {
		((Gtk.ListItem) item).selectable = true;
		var action_row = (Widgets.CommunityRow) ((Gtk.ListItem) item).child;
		var action_item = (API.Objects.Lemmy.CommunityExploreWrapper.CommunityExplore) ((Gtk.ListItem) item).item;

        var gtklistitemwidget = action_row.get_parent ();
		if (gtklistitemwidget != null) {
			gtklistitemwidget.add_css_class ("card");
			gtklistitemwidget.add_css_class ("card-spacing");
			//  gtklistitemwidget.focusable = true;
		}

        action_row.from_explore (action_item);
	}
}
