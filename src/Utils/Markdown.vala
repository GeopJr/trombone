public class Trombone.MarkdownUtils {
    public static string html_from_string (string content) {
        var md = new Markdown.Document.from_string (content.data, 0);
        md.compile (0);

		string html;
        md.document (out html);

        return html;
    }

    class HtmlHandler : Object {
        public delegate void NodeFn (Xml.Node* node);
	    public delegate void NodeHandlerFn (Xml.Node* node);
        public string res { get; private set; default=""; }

        public HtmlHandler.from_string (string content, bool less_newlines = false) {
            var doc = Html.Doc.read_doc (content, "", "utf8");
		    if (doc != null) {
		    	var root = doc->get_root_element ();
		    	if (root != null) {
		    		default_handler (root);
		    	}
		    }

		    delete doc;

            res = res.strip ();
            if (less_newlines) {
                while ("\n\n\n" in res) {
                    res = res.replace ("\n\n", "\n");
                }
            }
        }

        void traverse (Xml.Node* root, owned NodeFn cb) {
            for (var iter = root->children; iter != null; iter = iter->next) {
                cb (iter);
            }
        }

        void traverse_and_handle (Xml.Node* root, owned NodeHandlerFn handler) {
            traverse (root, node => {
                handler (node);
            });
        }

        void default_handler (Xml.Node* root) {
            switch (root->name) {
                case "span":
                case "html":
                case "markup":
                case "body":
                case "hr":
                    traverse_and_handle (root, default_handler);
                    break;
                case "p":
                    if (root->children == null && root->content == null) break;
                    res += "\n";
                    traverse_and_handle (root, default_handler);
                    res += "\n";
                    break;
                case "img":
                    var href = root->get_prop ("href");
                    var alt = root->get_prop ("href");
                    if (href != null) {
                        res += @"<a href='$(GLib.Markup.escape_text (href))'>$(alt ?? href)</a>";
                    }
                    break;
                case "pre":
                    if (
                        root->children != null
                        && root->children->name == "code"
                        && root->children->children != null
                    ) {
                        //  blockquote_handler_text = "";
                        //  blockquote_handler (root->children);
                        //  var text = blockquote_handler_text.strip ();
                        //  var label = new RichLabel (text) {
                        //  	visible = true,
                        //  	css_classes = { "ttl-code", "monospace" },
                        //  	use_markup = true
                        //  	// markup = MarkupPolicy.DISALLOW
                        //  };

                        //  v.append (label);

                        res += "<i><span background=\"#9696961a\" font_family=\"monospace\">";
                        traverse_and_handle (root, default_handler);
                        res += "</span></i>";
                    } else {
                        res += "\n";
                        traverse_and_handle (root, default_handler);
                        res += "\n";
                    }
                    break;
                case "code":
                    res += "<span background=\"#9696961a\" font_family=\"monospace\">";
                    traverse_and_handle (root, default_handler);
                    res += "</span>";
                    break;
                case "blockquote":
                        res += "<i><span background=\"#9696961a\" font_family=\"monospace\">";
                        traverse_and_handle (root, default_handler);
                        res += "</span></i>";
                    break;
                case "a":
                    var href = root->get_prop ("href");
                    if (href != null) {
                        res += @"<a href='$(GLib.Markup.escape_text (href))'>";
                        traverse_and_handle (root, default_handler);
                        res += "</a>";
                    }
                    break;
                case "h1":
                    if (!res.has_suffix ("\n"))
                        res += "\n";
                    res += "<b><span size=\"xx-large\">";
                    traverse_and_handle (root, default_handler);
                    res += "</span></b>\n";
                    break;
                case "h2":
                    if (!res.has_suffix ("\n"))
                        res += "\n";
                    res += "<b><span size=\"x-large\">";
                    traverse_and_handle (root, default_handler);
                    res += "</span></b>\n";
                    break;
                case "h3":
                    if (!res.has_suffix ("\n"))
                        res += "\n";
                    res += "<b><span size=\"large\">";
                    traverse_and_handle (root, default_handler);
                    res += "</span></b>\n";
                    break;
                case "h4":
                    if (!res.has_suffix ("\n"))
                        res += "\n";
                    res += "<b>";
                    traverse_and_handle (root, default_handler);
                    res += "</b>\n";
                    break;
                case "h5":
                    if (!res.has_suffix ("\n"))
                        res += "\n";
                    res += "<b><span size=\"small\">";
                    traverse_and_handle (root, default_handler);
                    res += "</span></b>\n";
                    break;
                case "h6":
                    if (!res.has_suffix ("\n"))
                        res += "\n";
                    res += "<b><span size=\"x-small\">";
                    traverse_and_handle (root, default_handler);
                    res += "</span></b>\n";
                    break;
                case "strong":
                    res += "<b>";
                    traverse_and_handle (root, default_handler);
                    res += "</b>";
                    break;
                case "em":
                    res += "<i>";
                    traverse_and_handle (root, default_handler);
                    res += "</i>";
                    break;
                case "del":
                    res += "<s>";
                    traverse_and_handle (root, default_handler);
                    res += "</s>";
                    break;
                case "b":
                case "i":
                case "u":
                case "s":
                case "sup":
                case "sub":
                    res += @"<$(root->name)>";
                    traverse_and_handle (root, default_handler);
                    res += @"</$(root->name)>";
                break;
                case "ol":
                    int li_count = 1;
                    for (var iter = root->children; iter != null; iter = iter->next) {
                        if (iter->name == "li") {
                            res += @"\n$li_count. ";
                            traverse_and_handle (iter, list_item_handler);

                            li_count++;
                        } else continue;
                    }
                    res += "\n";

                    break;
                case "ul":
                    traverse_and_handle (root, default_handler);
                    res += "\n";
                    break;
                case "li":
                    res += "\n• ";
                    traverse_and_handle (root, list_item_handler);
                    break;
                case "br":
                    res += "\n";
                    break;
                case "text":
                    if (root->content != null)
                        res += GLib.Markup.escape_text (root->content);
                    break;
                default:
                    warning (@"Unknown HTML tag: \"$(root->name)\"");
                    traverse_and_handle (root, default_handler);
                    break;
            }
        }

        void list_item_handler (Xml.Node* root) {
            switch (root->name) {
                case "p":
                    traverse_and_handle (root, default_handler);
                    break;
                default:
                    default_handler (root);
                    break;
            }
        }
    }

    public static string pango_from_html (string html, bool less_newlines = false) {
        return new HtmlHandler.from_string (html, less_newlines).res;
    }

    public static string pango_from_string (string content, bool less_newlines = false) {
        return pango_from_html (html_from_string (content), less_newlines);
    }
}
