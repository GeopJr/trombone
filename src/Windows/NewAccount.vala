[GtkTemplate (ui = "/dev/geopjr/Trombone/windows/new_account.ui")]
public class Trombone.Windows.NewAccount : Adw.Window {
    protected bool has_totp { get; set; default=false; }
    protected bool is_working { get; set; default=false; }
    private string instance { get; set; default=""; }
	private API.Backend.InstanceType instance_guess { get; set; }

    [GtkChild] unowned Adw.NavigationView deck;
	[GtkChild] unowned Adw.NavigationPage instance_step;
	[GtkChild] unowned Adw.NavigationPage login_step;
	[GtkChild] unowned Adw.NavigationPage done_step;

	[GtkChild] unowned Adw.EntryRow instance_entry;
	[GtkChild] unowned Gtk.Label instance_entry_error;

	[GtkChild] unowned Adw.EntryRow username_entry;
	[GtkChild] unowned Adw.PasswordEntryRow password_entry;
	[GtkChild] unowned Adw.EntryRow totp_entry;
	[GtkChild] unowned Gtk.Label login_error;

	[GtkChild] unowned Adw.StatusPage auth_page;
	[GtkChild] unowned Adw.StatusPage done_page;

	private Adw.EntryRow[] entries;
    private Gtk.Label[] entries_labels;
	public NewAccount () {
		Object (transient_for: app.main_window);
		app.add_account_window = this;
		app.add_window (this);

		entries = { username_entry, password_entry, totp_entry };
		entries_labels = { instance_entry_error, login_error };

		reset ();
		present ();
		instance_entry.grab_focus ();
	}

	//  private show_alpha_warning () {
	//  	new Adw.StatusPage () {
	//  		icon_name = "bug-symbolic",
	//  		title = _("This is alpha software"),
	//  		subtitle = _("You might encounter bugs and missing features")
	//  	}
	//  }

	private async void step () throws Error {
		if (deck.visible_page == instance_step) {
			setup_instance ();
			instance_guess = yield Trombone.API.Backend.guess (instance_entry.text);
			if (!instance_guess.kind.is_supported ()) {
				if (instance_guess.kind == API.Backend.Type.UNKNOWN) {
					throw new Oopsie.INTERNAL (_("Unsupported Server"));
				} else {
					throw new Oopsie.INTERNAL (_("%s is not currently supported.").printf (instance_guess.kind.to_string ()));
				}
			}

			deck.push (login_step);
		} else if (deck.visible_page == login_step) {
			try {
				bool is_api_v18 = false;
				if (instance_guess.version != "") {
					string[] version = instance_guess.version.split (".");
					is_api_v18 = version.length >= 2 && int.parse (version[1]) <= 18;
				}

				var account = yield API.Lemmy.login (
					instance_entry.text,
					username_entry.text,
					password_entry.text,
					totp_entry.text,
					is_api_v18
				);

				debug ("Saving account");
				accounts.add (account);

				done_page.title = _("Hello, %s!").printf (account.name);
				deck.push (done_step);

				debug ("Switching to account");
				accounts.activate (account, true);
			} catch (GLib.Error e) {
				if (e.message == "missing_totp_token" && !has_totp) {
					has_totp = true;
					return;
				}

				warning (e.message);
				throw new Oopsie.INTERNAL (e.message);
			}
		}
	}

    [GtkCallback] void on_next_clicked () {
		clear_errors ();
		if (is_working) return;

		is_working = true;
		step.begin ((obj, res) => {
			try {
				step.end (res);
				clear_errors ();
			} catch (Oopsie.INTERNAL e) {
				mark_errors (e.message);
			} catch (Oopsie.INSTANCE e) {
				oopsie (_("Server returned an error"), e.message);
				mark_errors (e.message);
			} catch (Error e) {
				oopsie (e.message);
				mark_errors (e.message);
			}
			is_working = false;
		});
	}

	void oopsie (string title, string msg = "") {
		warning (@"$title   $msg.");
		var dlg = app.inform (title, msg, this);
		dlg.present ();
	}

    [GtkCallback] void clear_errors () {
        instance_entry.remove_css_class ("error");
        foreach (var entry in entries) {
            entry.remove_css_class ("error");
        }

        foreach (var label in entries_labels) {
            label.label = "";
        }
    }

    void mark_errors (string error_message) {
		instance_entry.add_css_class ("error");
		instance_entry_error.label = error_message;

		foreach (var entry in entries) {
            entry.add_css_class ("error");
        }
		login_error.label = error_message;
	}

    void reset () {
		debug ("Reset state");
		clear_errors ();

        foreach (var entry in entries) {
            entry.text = "";
        }

		deck.pop_to_page (instance_step);
	}

    void setup_instance () throws Error {
		debug ("Checking instance URL");

		var str = instance_entry.text
			.replace ("/", "")
			.replace (":", "")
			.replace ("https", "")
			.replace ("http", "");
		instance = @"https://$str";
		instance_entry.text = str;

		if (str.char_count () <= 0 || !("." in instance))
			throw new Oopsie.USER (_("Please enter a valid instance URL"));
	}

    [GtkCallback] void on_done_clicked () {
        app.present_window ();
		destroy ();
    }

    [GtkCallback] void on_back_clicked () {
        reset ();
    }
}
