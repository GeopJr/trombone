[GtkTemplate (ui = "/dev/geopjr/Trombone/windows/sidebar/view.ui")]
public class Trombone.Views.Sidebar : Gtk.Widget, AccountHolder {
	[GtkChild] unowned Gtk.Popover account_switcher_popover_menu;
	[GtkChild] unowned Gtk.ListBox saved_accounts;
	[GtkChild] unowned Widgets.Avatar accounts_button_avi;
	[GtkChild] unowned Gtk.MenuButton menu_btn;
	[GtkChild] unowned Adw.ClampScrollable viewport;

	protected API.InstanceAccount? account { get; set; default = null; }

	protected Gtk.SliceListModel account_items;
	protected GLib.ListStore accounts_model;
	protected Gtk.ListView content;
	protected GLib.ListStore model;
	protected Gtk.SingleSelection no_selection;

	static construct {
		set_layout_manager_type (typeof (Gtk.BinLayout));
	}

	construct {
		model = new GLib.ListStore (typeof (Widgets.SidebarItem.Item));
		Gtk.SignalListItemFactory signallistitemfactory = new Gtk.SignalListItemFactory ();
		signallistitemfactory.setup.connect (setup_listitem_cb);
		signallistitemfactory.bind.connect (bind_listitem_cb);

		no_selection = new Gtk.SingleSelection (model);
		content = new Gtk.ListView (no_selection, signallistitemfactory) {
			css_classes = { "ttl-listview", "navigation-sidebar" },
			single_click_activate = false
		};
		viewport.child = content;

		no_selection.notify["selected"].connect (on_content_item_activated);

		var menu_model = new GLib.Menu ();

		accounts_model = new GLib.ListStore (typeof (Object));
		saved_accounts.bind_model (accounts_model, on_accounts_row_create);

		var account_submenu_model = new GLib.Menu ();
		account_submenu_model.append (_("Open Profile"), "app.open-current-account-profile");
		account_submenu_model.append (_("Refresh"), "app.refresh");
		menu_model.append_section (null, account_submenu_model);

		var misc_submenu_model = new GLib.Menu ();
		misc_submenu_model.append (_("Preferences"), "app.open-preferences");
		misc_submenu_model.append (_("Keyboard Shortcuts"), "win.show-help-overlay");
		misc_submenu_model.append (_("About %s").printf (Build.NAME), "app.about");
		menu_model.append_section (null, misc_submenu_model);

		menu_btn.menu_model = menu_model;

		account_items = new Gtk.SliceListModel (null, 0, 15);
		saved_accounts.set_header_func (on_account_header_update);

		construct_account_holder ();
	}

	protected virtual void setup_listitem_cb (GLib.Object item) {
		((Gtk.ListItem) item).child = new Widgets.SidebarItem ();
	}

	protected virtual void bind_listitem_cb (GLib.Object item) {
		((Gtk.ListItem) item).selectable = true;
		var sidebar_child = (Widgets.SidebarItem) ((Gtk.ListItem) item).child;
		var sidebar_item = (Widgets.SidebarItem.Item) ((Gtk.ListItem) item).item;

		var gtklistitemwidget = ((Gtk.ListItem) item).child.get_parent ();
		if (gtklistitemwidget != null) {
			if (sidebar_item.separator) gtklistitemwidget.add_css_class ("ttl-fake-separator");
		}
		sidebar_child.for_item (sidebar_item);
	}

	public virtual void on_content_item_activated () {
		if (app.main_window == null) return;

		var selected_item = (Widgets.SidebarItem.Item) no_selection.selected_item;
		if (selected_item.open_func != null) {
			selected_item.open_func ();
		} else if (selected_item.community != null) {
			app.main_window.open_view (new Views.Community (selected_item.community));
		}
	}

	public void set_selected_item (int index) {
		no_selection.selected = index;
	}

	public virtual Gtk.Widget on_accounts_row_create (Object obj) {
		var row = new AccountRow (obj as API.InstanceAccount);
		row.popdown_signal.connect (popdown);

		return row;
	}

	private int base_items_count = 0;
	protected virtual void on_accounts_changed (Gee.ArrayList<API.InstanceAccount> accounts) {
		accounts_model.remove_all ();

		Object[] accounts_to_add = {};
		accounts.foreach (acc => {
			accounts_to_add += acc;

			return true;
		});
		accounts_to_add += new Object ();

		Widgets.SidebarItem.Item[] to_add_static = {
			new Widgets.SidebarItem.Item () {
				basic = { "Home", "go-home-symbolic" },
				open_func = () => {
					app.main_window.go_back_to_start ();
				}
			},
			new Widgets.SidebarItem.Item () {
				basic = { "Inbox", "mail-unread-symbolic" }
			},
			new Widgets.SidebarItem.Item () {
				basic = { "Search", "folder-saved-search-symbolic" }
			},
			new Widgets.SidebarItem.Item () {
				basic = { "Saved", "bookmark-outline-symbolic" },
				separator = true,
				open_func = () => {
					app.main_window.open_view (new Views.Saved () {
						is_sidebar_item = true
					});
				}
			},
			new Widgets.SidebarItem.Item () {
				basic = { "Explore", "explore2-symbolic" },
				open_func = () => {
					app.main_window.open_view (new Views.Explore () {
						is_sidebar_item = true
					});
				}
			},
			new Widgets.SidebarItem.Item () {
				basic = { "Local", "network-server-symbolic" },
				open_func = () => {
					app.main_window.open_view (new Views.Local () {
						is_sidebar_item = true
					});
				}
			},
			new Widgets.SidebarItem.Item () {
				basic = { "Federated", "globe-symbolic" },
				separator = true,
				open_func = () => {
					app.main_window.open_view (new Views.Federated () {
						is_sidebar_item = true
					});
				}
			}
		};
		base_items_count = to_add_static.length;
		model.splice (0, 0, to_add_static);

		app.update_communities.connect (fill_communities);
		fill_communities ();

		accounts_model.splice (0, 0, accounts_to_add);
		update_selected_account ();
	}

	private void fill_communities () {
		if (Trombone.accounts.active == null || Trombone.accounts.active.instance_info == null || Trombone.accounts.active.instance_info.my_user.follows == null) return;

		string? selected_item_handle = null;
		if (no_selection.selected >= base_items_count) selected_item_handle = ((Widgets.SidebarItem.Item) no_selection.selected_item)?.community?.handle;

		Widgets.SidebarItem.Item[] to_add = {};

		int pos = 0;
		foreach (var community_wrapper in Trombone.accounts.active.instance_info.my_user.follows) {
			to_add += new Widgets.SidebarItem.Item () {
				community = community_wrapper.community
			};

			if (selected_item_handle != null && community_wrapper.community.handle == selected_item_handle) no_selection.selected = base_items_count + pos;
			pos++;
		}

		model.splice (base_items_count, model.n_items - base_items_count, to_add);
		to_add = {};
	}

	private void update_selected_account () {
		uint index;
		if (accounts_model.find (account, out index))
			saved_accounts.select_row (saved_accounts.get_row_at_index ((int) index));
	}

	private Binding sidebar_avatar_btn;
	private Binding sidebar_avatar_btn_fallback;
	private bool has_bindings = false;
	protected virtual void on_account_changed (API.InstanceAccount? account) {
		if (this.account != null && has_bindings) {
			sidebar_avatar_btn.unbind ();
			sidebar_avatar_btn_fallback.unbind ();
			has_bindings = false;
		}

		if (app?.main_window != null)
			app.main_window.go_back_to_start ();

		this.account = account;

		if (account != null) {
			sidebar_avatar_btn_fallback = this.account.bind_property ("name", accounts_button_avi, "fallback_text", BindingFlags.SYNC_CREATE);
			sidebar_avatar_btn = this.account.bind_property ("avatar", accounts_button_avi, "avatar-url", BindingFlags.SYNC_CREATE);
			update_selected_account ();
			has_bindings = true;
		} else {
			saved_accounts.unselect_all ();

			account_items.model = null;
			accounts_button_avi.account = null;
		}
	}

	// Account
	[GtkTemplate (ui = "/dev/geopjr/Trombone/windows/sidebar/account.ui")]
	protected class AccountRow : Adw.ActionRow {
		public API.InstanceAccount? account;

		[GtkChild] unowned Widgets.Avatar avatar;
		[GtkChild] unowned Gtk.Button forget;

		public signal void popdown_signal ();

		private Binding switcher_display_name;
		private Binding switcher_handle;
		private Binding switcher_tooltip;
		private Binding switcher_avatar;
		private Binding switcher_avatar_fallback;
		private bool has_bindings = false;
		public AccountRow (API.InstanceAccount? _account) {
			if (account != null && has_bindings) {
				switcher_display_name.unbind ();
				switcher_handle.unbind ();
				switcher_tooltip.unbind ();
				switcher_avatar.unbind ();
				switcher_avatar_fallback.unbind ();
				has_bindings = false;
			}

			account = _account;
			if (account != null) {
				switcher_display_name = this.account.bind_property ("name", this, "title", BindingFlags.SYNC_CREATE);
				switcher_handle = this.account.bind_property ("handle", this, "subtitle", BindingFlags.SYNC_CREATE);
				switcher_tooltip = this.account.bind_property ("handle", this, "tooltip-text", BindingFlags.SYNC_CREATE);
				switcher_avatar = this.account.bind_property ("avatar", avatar, "avatar-url", BindingFlags.SYNC_CREATE);
				switcher_avatar_fallback = this.account.bind_property ("name", avatar, "fallback_text", BindingFlags.SYNC_CREATE);
				has_bindings = true;
			} else {
				title = _("Add Account");
				avatar.account = null;
				selectable = false;
				forget.hide ();
				tooltip_text = _("Add Account");
				avatar.icon_name = "plus-large-symbolic";
				avatar.remove_css_class ("flat");
			}
		}

		[GtkCallback] void on_open () {
			if (account != null) {
				//  account.resolve_open (accounts.active);
			} else {
				new Windows.NewAccount ().present ();
			}
			popdown_signal ();
		}

		[GtkCallback] void on_forget () {
			popdown_signal ();
			app.question.begin (
				// translators: the variable is an account handle
				{_("Forget %s?").printf ("<span segment=\"word\">@%s</span><span segment=\"word\">@%s</span>".printf (account.name, account.instance)), true},
				{_("This account will be removed from the application."), false},
				app.main_window,
				{ { _("Forget"), Adw.ResponseAppearance.DESTRUCTIVE }, { _("Cancel"), Adw.ResponseAppearance.DEFAULT } },
				false,
				(obj, res) => {
					if (app.question.end (res).truthy ()) {
						try {
							accounts.remove (account);
						} catch (Error e) {
							warning (e.message);
							var dlg = app.inform (_("Error"), e.message);
							dlg.present ();
						}
					}
				}
			);
		}
	}

	void popdown () {
		account_switcher_popover_menu.popdown ();
	}

	void on_account_header_update (Gtk.ListBoxRow _row, Gtk.ListBoxRow? _before) {
		var row = _row as AccountRow;

		row.set_header (null);

		if (row.account == null && _before != null)
			row.set_header (new Gtk.Separator (Gtk.Orientation.HORIZONTAL));
	}

	[GtkCallback] void on_account_activated (Gtk.ListBoxRow _row) {
		popdown ();

		var row = _row as AccountRow;
		if (row.account != null)
			accounts.activate (row.account, true);
		else
			new Windows.NewAccount ().present ();
	}

}
