[GtkTemplate (ui = "/dev/geopjr/Trombone/windows/main_window.ui")]
public class Trombone.Windows.MainWindow : Adw.ApplicationWindow, Saveable {
    [GtkChild] unowned Adw.NavigationView navigation_view;
	[GtkChild] public unowned Adw.OverlaySplitView split_view;
	[GtkChild] unowned Views.Sidebar sidebar;
	//  [GtkChild] unowned Gtk.Stack main_stack;
	[GtkChild] unowned Views.MediaViewer media_viewer;
	[GtkChild] unowned Adw.Breakpoint breakpoint;
	[GtkChild] unowned Adw.ToastOverlay toast_overlay;

	public bool is_mobile { get; set; default = false; }
	public bool is_media_viewer_visible {
		get { return media_viewer.visible; }
	}

	public void scroll_media_viewer (int pos) {
		if (!is_media_viewer_visible) return;

		media_viewer.scroll_to (pos);
	}

	construct {
		construct_saveable (settings);

		var gtk_settings = Gtk.Settings.get_default ();
		breakpoint.add_setter (this, "is-mobile", true);

		app.toast.connect (add_toast);
		media_viewer.bind_property ("visible", split_view, "can-focus", GLib.BindingFlags.SYNC_CREATE | GLib.BindingFlags.INVERT_BOOLEAN);
		media_viewer.notify["visible"].connect (on_media_viewer_toggle);
		navigation_view.popped.connect (on_pop);
	}

	private void add_toast (string content, uint timeout = 0) {
		toast_overlay.add_toast (new Adw.Toast (content) {
			timeout = timeout
		});
	}

	private weak Gtk.Widget? media_viewer_source_widget;
	private void on_media_viewer_toggle () {
		if (is_media_viewer_visible || media_viewer_source_widget == null) return;

		Gtk.Widget focusable_widget = media_viewer_source_widget;
		while (focusable_widget != null && !focusable_widget.focusable) focusable_widget = focusable_widget.get_parent ();
		if (focusable_widget != null) focusable_widget.grab_focus ();
		media_viewer_source_widget = null;
	}

	public void show_media_viewer (
		string url,
		Trombone.Attachment.MediaType media_type,
		Gdk.Paintable? preview,
		int? pos = null,
		Gtk.Widget? source_widget = null,
		bool as_is = false,
		string? alt_text = null,
		string? user_friendly_url = null,
		bool stream = false
	) {
		if (as_is && preview == null) return;

		media_viewer.add_media (url, media_type, preview, pos, as_is, alt_text, user_friendly_url, stream, source_widget);

		if (!is_media_viewer_visible) {
			media_viewer.reveal (source_widget);
			media_viewer_source_widget = source_widget;
		}
	}

	public bool back () {
		if (is_media_viewer_visible) {
			media_viewer.clear ();
			return true;
		};

		navigation_view.pop ();
		return true;
	}

	Views.Home main_page;
    public MainWindow (Adw.Application app) {
		Object (
			application: app,
			icon_name: Build.DOMAIN,
			title: Build.NAME,
			resizable: true
		);

		main_page = new Trombone.Views.Home ();
		navigation_view.push (main_page);
		split_view.bind_property ("collapsed", main_page, "sidebar-collapsed", GLib.BindingFlags.SYNC_CREATE);
		main_page.sidebar_toggled.connect (on_sidebar_toggle);
	}

	private void on_sidebar_toggle () {
		split_view.show_sidebar = true;
	}

	int n_pages = 1;
	public void open_view (Views.Base view) {
		if (view.is_sidebar_item) {
			navigation_view.replace ({ main_page, view });
			n_pages = 2;
			return;
		}

		n_pages += 1;
		bool found_page = navigation_view.find_page (view.tag) != null;
		if (found_page) view.tag = @"$(view.tag)$(n_pages)";

		navigation_view.push (view);
	}

	private void on_pop (Adw.NavigationPage page) {
		n_pages -= 1;
		if (n_pages == 1) sidebar.set_selected_item (0);
	}

	public void go_back_to_start () {
		if (n_pages == 1) return;
		navigation_view.replace ({ main_page });
		n_pages = 1;
	}
}
