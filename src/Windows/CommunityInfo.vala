[GtkTemplate (ui = "/dev/geopjr/Trombone/windows/community_info.ui")]
public class Trombone.Windows.CommunityInfo : Adw.Window {
	[GtkChild] unowned Widgets.Avatar avatar;
	[GtkChild] unowned Gtk.Label com_name;
	[GtkChild] unowned Gtk.Label handle;
	[GtkChild] unowned Gtk.Label counters;
	[GtkChild] unowned Gtk.Label description;
	[GtkChild] unowned Adw.PreferencesGroup mod_group;
	[GtkChild] unowned Gtk.ListBox mod_list;
	[GtkChild] unowned Gtk.Picture background;

	public CommunityInfo (API.Objects.Lemmy.CommunityInfoWrapper community_info) {
		Object (transient_for: app.main_window);

		if (community_info.community_view.community.icon != null && community_info.community_view.community.icon != "") {
			avatar.avatar_url = community_info.community_view.community.icon;
		} else {
			avatar.fallback_text = community_info.community_view.community.title;
		}

		com_name.label = community_info.community_view.community.title;
		handle.label = community_info.community_view.community.handle;
		counters.label = @"<b>$(Trombone.Units.shorten (community_info.community_view.counts.posts))</b> Posts · <b>$(Trombone.Units.shorten (community_info.community_view.counts.subscribers))</b> Subscribers";
		counters.tooltip_text = @"$(community_info.community_view.counts.posts) Posts $(community_info.community_view.counts.subscribers) Subscribers";
		//  description.label = community_info.community_view.community.description;
		description.label = MarkdownUtils.pango_from_string (GLib.Markup.escape_text (community_info.community_view.community.description), true);

		if (community_info.community_view.community.banner != null && community_info.community_view.community.banner != "") {
			Trombone.Helper.Image.request_paintable (community_info.community_view.community.banner, null, null, on_bg_cache_response);
		}

		if (community_info.moderators != null && community_info.moderators.size > 0) {
			mod_group.visible = true;

			foreach (var moderator in community_info.moderators) {
				var row = new Adw.ActionRow () {
					title = moderator.moderator.name,
					subtitle = moderator.moderator.handle,
					subtitle_selectable = true
				};
				var avi = new Widgets.Avatar () {
					size = 32,
					can_focus = false,
					can_target = false
				};

				if (moderator.moderator.avatar != null && moderator.moderator.avatar != "") {
					avi.avatar_url = moderator.moderator.avatar;
				} else {
					avi.fallback_text = moderator.moderator.name;
				}

				row.add_prefix (avi);
				mod_list.append (row);
			}
		}

		present ();

		// HACK if done before presented everything will be selected
		description.selectable = true;
	}

	void on_bg_cache_response (Gdk.Paintable? data) {
		background.paintable = data;
	}
}
