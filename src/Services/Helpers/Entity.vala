public class Trombone.Helper.Entity {
	public static Trombone.Entity from_json (owned Json.Node node, owned Type type, bool force = false) {
		Trombone.Entity entity = null;

		try {
			entity = Trombone.Entity.from_json (type, node);
		} catch (Error e) {
			warning (@"Error getting Entity from json: $(e.message)");
		}

		return entity;
	}
}
