public abstract class Trombone.AccountStore : GLib.Object {
	public Gee.ArrayList<API.InstanceAccount> saved { get; set; default = new Gee.ArrayList<API.InstanceAccount> (); }
	public API.InstanceAccount? active { get; set; default = null; }

	public signal void changed (Gee.ArrayList<API.InstanceAccount> accounts);
	public signal void switched (API.InstanceAccount? account);

	public bool ensure_active_account () {
		var has_active = false;
		var account = find_by_uuid (settings.active_account);
        var clear_cache = false;

		if (account == null && !saved.is_empty) {
			account = saved[0];
            clear_cache = true;
		}

		has_active = account != null;
		activate (account, clear_cache);

		if (!has_active)
			app.present_window (true);

		return has_active;
	}

	public virtual void init () throws GLib.Error {
		load ();
		ensure_active_account ();
	}

	public abstract void load () throws GLib.Error;
	public abstract void save (API.InstanceAccount? t_acc = null) throws GLib.Error;
	public void safe_save (API.InstanceAccount? t_acc = null) {
		try {
			save (t_acc);
		} catch (GLib.Error e) {
			warning (e.message);
			var dlg = app.inform (_("Error"), e.message);
			dlg.present ();
		}
	}

	public virtual void add (API.InstanceAccount account) throws GLib.Error {
		debug (@"Adding new account: $(account.handle)");
		saved.add (account);
		changed (saved);
		save ();
		ensure_active_account ();
	}

	public virtual void remove (API.InstanceAccount account) throws GLib.Error {
		debug (@"Removing account: $(account.handle)");
		account.removed ();
		saved.remove (account);
		changed (saved);
		save ();
		ensure_active_account ();
	}

	public API.InstanceAccount? find_by_uuid (string uuid) {
		if (!GLib.Uuid.string_is_valid (uuid)) return null;
		var iter = saved.filter (acc => {
			return acc.uuid == uuid;
		});
		iter.next ();

		if (!iter.valid)
			return null;
		else
			return iter.@get ();
	}

	public void activate (API.InstanceAccount? account, bool clear_cache = false) {
		if (active != null)
			active.deactivated ();

		if (account == null) {
			debug ("Reset active account");
			return;
		} else {
			debug (@"Activating $(account.handle)…");
			if (clear_cache)
				network.clear_cache ();
			account.verify_credentials.begin ((obj, res) => {
				try {
					account.verify_credentials.end (res);
					settings.active_account = account.uuid;
					app.update_communities ();
				} catch (Error e) {
					warning (@"Couldn't activate account $(account.handle):");
					warning (e.message);
				}
			});
		}

		accounts.active = account;
		active.activated ();
		switched (active);
	}
}
