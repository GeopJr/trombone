public class Trombone.SecretAccountStore : AccountStore {
	const string VERSION = "1";

	Secret.Schema schema;
	GLib.HashTable<string,Secret.SchemaAttributeType> schema_attributes;

	public override void init () throws GLib.Error {
		debug (@"Using libsecret v$(Secret.MAJOR_VERSION).$(Secret.MINOR_VERSION).$(Secret.MICRO_VERSION)");

		schema_attributes = new GLib.HashTable<string,Secret.SchemaAttributeType> (str_hash, str_equal);
		schema_attributes["login"] = Secret.SchemaAttributeType.STRING;
		schema_attributes["version"] = Secret.SchemaAttributeType.STRING;
		schema_attributes["app"] = Secret.SchemaAttributeType.STRING;
		schema = new Secret.Schema.newv (
			Build.DOMAIN,
			Secret.SchemaFlags.DONT_MATCH_NAME,
			schema_attributes
		);

		base.init ();
	}

	public override void load () throws GLib.Error {
		var attrs = new GLib.HashTable<string,string> (str_hash, str_equal);
		attrs["version"] = VERSION;
		attrs["app"] = Build.DOMAIN;

		List<Secret.Retrievable> secrets = new List<Secret.Retrievable> ();
		try {
			secrets = Secret.password_searchv_sync (
				schema,
				attrs,
				Secret.SearchFlags.ALL | Secret.SearchFlags.UNLOCK,
				null
			);
		} catch (GLib.Error e) {
			string wiki_page = "https://github.com/GeopJr/Tuba/wiki/keyring-issues";

			// Let's leave this untranslated for now
			string help_msg = "If you didn’t manually cancel it, try creating a password keyring named \"login\" using Passwords and Keys (seahorse) or KWalletManager";

			if (e.message == "org.freedesktop.DBus.Error.ServiceUnknown") {
				wiki_page = "https://github.com/GeopJr/Tuba/wiki/libsecret-issues";
				help_msg = @"$(e.message), $(Build.NAME) might be missing some permissions";
			}

			critical (@"Error while searching for items in the secret service: $(e.message)");
			warning (@"$help_msg\nread more: $wiki_page");

			//  new Dialogs.NewAccount ();
			//  app.question.begin (
			//  	{"Error while searching for user accounts", false},
			//  	{@"$help_msg.", false},
			//  	app.add_account_window,
			//  	{ {"Read More", Adw.ResponseAppearance.SUGGESTED }, { "Close", Adw.ResponseAppearance.DEFAULT } },
			//  	false,
			//  	(obj, res) => {
			//  		if (app.question.end (res).truthy ()) Host.open_uri (wiki_page);
			//  		Process.exit (1);
			//  	}
			//  );
		}

		secrets.foreach (item => {
			var account = secret_to_account (item);
			if (account != null && account.handle != "") {
				saved.add (account);
				account.added ();
			}
		});
		changed (saved);

		debug (@"Loaded $(saved.size) accounts");
	}

	public override void save (API.InstanceAccount? t_acc = null) throws GLib.Error {
		if (t_acc != null) {
			account_to_secret (t_acc);
			debug (@"Saved $(t_acc.handle)");
			return;
		}

		saved.foreach (account => {
			account_to_secret (account);
			return true;
		});
		debug (@"Saved $(saved.size) accounts");
	}

	public override void remove (API.InstanceAccount account) throws GLib.Error {
		base.remove (account);

		var attrs = new GLib.HashTable<string,string> (str_hash, str_equal);
		attrs["version"] = VERSION;
		attrs["login"] = account.handle;
		attrs["app"] = Build.DOMAIN;

		Secret.password_clearv.begin (
			schema,
			attrs,
			null,
			(obj, async_res) => {
				try {
					Secret.password_clearv.end (async_res);
				}
				catch (GLib.Error e) {
					warning (e.message);
					var dlg = app.inform (_("Error"), e.message);
					dlg.present ();
				}
			}
		);
	}

	void account_to_secret (API.InstanceAccount account) {
		var attrs = new GLib.HashTable<string,string> (str_hash, str_equal);
		attrs["login"] = account.handle;
		attrs["version"] = VERSION;
		attrs["app"] = Build.DOMAIN;

		var generator = new Json.Generator ();
        var builder = account.to_secret ();
		generator.set_root (builder.get_root ());
		var secret = generator.to_data (null);
		// translators: The variable is the backend like "Lemmy"
		var label = _("%s Account").printf (account.instance_type.to_string ());

		Secret.password_storev.begin (
			schema,
			attrs,
			Secret.COLLECTION_DEFAULT,
			label,
			secret,
			null,
			(obj, async_res) => {
				try {
					Secret.password_store.end (async_res);
					debug (@"Saved secret for $(account.handle)");
				} catch (GLib.Error e) {
					warning (e.message);
					var dlg = app.inform (_("Error"), e.message);
					dlg.present ();
				}
			}
		);
	}

	API.InstanceAccount? secret_to_account (Secret.Retrievable item) {
		API.InstanceAccount? account = null;

		try {
			var secret = item.retrieve_secret_sync ();
			var contents = secret.get_text ();
			var parser = new Json.Parser ();
			parser.load_from_data (contents, -1);

			account = API.InstanceAccount.from (parser.get_root ());
		} catch (GLib.Error e) {
			warning (e.message);
		}
		return account;
	}
}
